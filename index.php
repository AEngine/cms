<?php declare(strict_types=1);

require_once(__DIR__ . '/engine/init.php');

/* setup each loaded module */
CMSModule::setup();

/* run application */
if ($app->isDebug()) {
    $response = $app->run(true);

    register_shutdown_function(function () use ($app, $response) {
        $time   = round(microtime(true) - $_SERVER['REQUEST_TIME_FLOAT'], 7);
        $memory = str_convert_size(memory_get_usage());

        /**
         * @var \Doctrine\ORM\EntityManager $em
         */
        $em = app()->entityManager;
        $dbStack = $em->getConfiguration()->getSQLLogger();
        $dbQueries = count($dbStack->queries);
        $dbTime = 0;

        foreach ($dbStack->queries as $query) {
            $dbTime += $query['executionMS'];
        }

        $response = $response
            ->withAddedHeader('X-Memory', $memory)
            ->withAddedHeader('X-Time', $time . ' ms')
            ->withAddedHeader('X-DB-Queries', $dbQueries)
            ->withAddedHeader('X-DB-Execution', round($dbTime, 7) . ' ms');

        $app->respond($response);
    });

    exit;
}

$app->run();
