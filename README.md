OmniCMS structure
====
Structure for OmniCMS based on Orchid Framework.


#### Requirements
- PHP >= 7.0

#### Installation from Composer

Run this command from the directory in which you want to install.

```
composer create-project aengine/omnicms [my-app-name]
```

Replace `[my-app-name]` with the desired directory name for your new application.  

#### Manual installation

Copy project files into web directory, run this commands:
```
# composer install
# chmod 0755 ./addons
# chmod 0555 ./engine
# chmod 0777 ./var
# chmod 0777 ./var/cache
# chmod 0777 ./var/log
# chmod 0755 ./themes
# chmod 0777 ./uploads
# engine/libs/bin/doctrine orm:schema-tool:create
# chmod 0777 ./engine/var/log/db.sqlite
```

##### Update ORM

Run this command:
```
engine/libs/bin/doctrine orm:schema-tool:update --force
```
