
(function ($) {
    "use strict";
    
    /*[ Back to top ]
    ===========================================================*/
    var windowH = $(window).height()/2;

    $(window).on('scroll',function(){
        if ($(this).scrollTop() > windowH) {
            $("#myBtn").css('display','flex');
        } else {
            $("#myBtn").css('display','none');
        }
    });

    $('#myBtn').on("click", function(){
        $('html, body').animate({scrollTop: 0}, 300);
    });


    /*[ Show header dropdown ]
    ===========================================================*/
    $('.js-show-header-dropdown').on('click', function(){
        $(this).parent().find('.header-dropdown')
    });

    var menu = $('.js-show-header-dropdown');
    var sub_menu_is_showed = -1;

    for(var i=0; i<menu.length; i++){
        $(menu[i]).on('click', function(){
            
                if(jQuery.inArray( this, menu ) == sub_menu_is_showed){
                    $(this).parent().find('.header-dropdown').toggleClass('show-header-dropdown');
                    sub_menu_is_showed = -1;
                }
                else {
                    for (var i = 0; i < menu.length; i++) {
                        $(menu[i]).parent().find('.header-dropdown').removeClass("show-header-dropdown");
                    }

                    $(this).parent().find('.header-dropdown').toggleClass('show-header-dropdown');
                    sub_menu_is_showed = jQuery.inArray( this, menu );
                }
        });
    }

    $(".js-show-header-dropdown, .header-dropdown").click(function(event){
        event.stopPropagation();
    });

    $(window).on("click", function(){
        for (var i = 0; i < menu.length; i++) {
            $(menu[i]).parent().find('.header-dropdown').removeClass("show-header-dropdown");
        }
        sub_menu_is_showed = -1;
    });


     /*[ Fixed Header ]
    ===========================================================*/
    var posWrapHeader = $('.topbar').height();
    var header = $('.container-menu-header');

    $(window).on('scroll',function(){

        if($(this).scrollTop() >= posWrapHeader) {
            $('.header1').addClass('fixed-header');
            $(header).css('top',-posWrapHeader);

        }
        else {
            var x = - $(this).scrollTop();
            $(header).css('top',x);
            $('.header1').removeClass('fixed-header');
        }

        if($(this).scrollTop() >= 200 && $(window).width() > 992) {
            $('.fixed-header2').addClass('show-fixed-header2');
            $('.header2').css('visibility','hidden');
            $('.header2').find('.header-dropdown').removeClass("show-header-dropdown");
            
        }
        else {
            $('.fixed-header2').removeClass('show-fixed-header2');
            $('.header2').css('visibility','visible');
            $('.fixed-header2').find('.header-dropdown').removeClass("show-header-dropdown");
        }

    });
    
    /*[ Show menu mobile ]
    ===========================================================*/
    $('.btn-show-menu-mobile').on('click', function(){
        $(this).toggleClass('is-active');
        $('.wrap-side-menu').slideToggle();
    });

    var arrowMainMenu = $('.arrow-main-menu');

    for(var i=0; i<arrowMainMenu.length; i++){
        $(arrowMainMenu[i]).on('click', function(){
            $(this).parent().find('.sub-menu').slideToggle();
            $(this).toggleClass('turn-arrow');
        })
    }

    $(window).resize(function(){
        if($(window).width() >= 992){
            if($('.wrap-side-menu').css('display') == 'block'){
                $('.wrap-side-menu').css('display','none');
                $('.btn-show-menu-mobile').toggleClass('is-active');
            }
            if($('.sub-menu').css('display') == 'block'){
                $('.sub-menu').css('display','none');
                $('.arrow-main-menu').removeClass('turn-arrow');
            }
        }
    });
    
    /*[ +/- num product ]
    ===========================================================*/
    $(document)
        .on('click', '.btn-num-product-up', function (e) {
            e.preventDefault();
            var numProduct = Number($(this).prev().val());
            $(this).prev().val(numProduct + 1);
        })
        .on('click', '.btn-num-product-down', function (e) {
            e.preventDefault();
            var numProduct = Number($(this).next().val());
            if (numProduct > 1) $(this).next().val(numProduct - 1);
        });

    /*[ Show content Product detail ]
    ===========================================================*/
    $('.active-dropdown-content .js-toggle-dropdown-content').toggleClass('show-dropdown-content');
    $('.active-dropdown-content .dropdown-content').slideToggle('fast');

    $('.js-toggle-dropdown-content').on('click', function(){
        $(this).toggleClass('show-dropdown-content');
        $(this).parent().find('.dropdown-content').slideToggle('fast');
    });
    
    /*[ Polyfills ]
    ===========================================================*/
    /**
     * Функция возвращает окончание для множественного числа слова на основании числа и массива окончаний
     *
     * @param one string например яблоко
     * @param two string напримет яблока
     * @param five string например яблок
     *
     * @return string
     */
    String.prototype.eos = Number.prototype.eos = function (one, two, five) {
        if (!isNaN(+this)) {
            let value = this % 100;
            
            if (value >= 11 && value <= 19) {
                return five;
            } else {
                value = this % 10;
                
                switch (value) {
                    case (1):
                        return one;
                    case (2):
                    case (3):
                    case (4):
                        return two;
                    default:
                        return five;
                }
            }
        }
        
        return this;
    };
    
    /**
     * Сравнение объектов
     *
     * @param x
     * @param y
     * @returns {boolean}
     */
    Object.equals = function (x, y) {
        if (x === y) { return true; }
        if (!(x instanceof Object) || !(y instanceof Object)) { return false; }
        if (x.constructor !== y.constructor) { return false; }
        
        for (var p in x) {
            if (!x.hasOwnProperty(p)) { continue; }
            if (!y.hasOwnProperty(p)) { return false; }
            if (x[p] === y[p]) { continue; }
            if (typeof(x[p]) !== "object") { return false; }
            if (!Object.equals(x[p], y[p])) { return false; }
        }
        for (p in y) { if (y.hasOwnProperty(p) && !x.hasOwnProperty(p)) { return false; } }
        
        return true;
    };
    
    /*[ Поиск ]
    ===========================================================*/
    
    let
        $searchArea = $('.search-product'),
        $searchField = $('[name="search-product"]'),
        $searchButton = $('[name="search-product"] + button'),
        $searchResult = $('.search-result'),
        $searchResultLine = $('.search-result li').detach();
    
    let handler = () => {
        let query = $searchField.val();
    
        if (query.length >= 3) {
            $searchResult.show();
            $searchResult.html('');
        
            $.post('/search/' + query, (res) => {
                if (res.docsTotal) {
                    for (let index in res.documents) {
                        if (!res.documents.hasOwnProperty(index)) { continue; }
                    
                        let el = res.documents[index],
                            $buf = $searchResultLine.clone();
                    
                        $buf
                            .find('a')
                            .attr('href', el.url)
                            .text(el.name);
                    
                        $buf.appendTo($searchResult);
                    }
                }
            });
        }
    };
    
    $searchButton.on('click', handler);
    $searchField.on('keyup', (e) => { if (e.keyCode === 13) handler(); });
    
    /*[ SimpleCart ]
    ===========================================================*/
    simpleCart({
        checkout: {
            type: 'SendForm',
            url: '/cart',
            extra_data: {},
            
        },
        cartColumns: [
            {
                label: '',
                attr: 'thumb',
                view: (item) => { return '<div class="cart-img-product b-rad-4 o-f-hidden" data-cart-item-id="' + item.get('id') + '"><img src="' + item.get('thumb') + '"></div>'; },
            },
            {
                label: 'Нименование',
                attr: 'link',
                view: (item) => { return "<a href='" + item.get('link') + "'>" + item.get('name') + "</a>"; },
            },
            {
                label: 'Цена',
                attr: 'price',
                view: 'currency',
            },
            {
                label: 'Количество',
                attr: 'quantity',
                view: (item) => {
                    return '<div class="flex-w bo5 of-hidden w-size17" data-cart-item-id="' + item.get('id') + '">\n' +
                           '    <button class="cart-item-decrement color1 flex-c-m size7 bg8 eff2">\n' +
                           '        <i class="fs-12 fa fa-minus" aria-hidden="true"></i>\n' +
                           '    </button>\n' +
                           '    \n' +
                           '    <input class="size8 m-text18 t-center num-product" type="number" value="' + item.get('quantity') + '">\n' +
                           '    \n' +
                           '    <button class="cart-item-increment color1 flex-c-m size7 bg8 eff2">\n' +
                           '        <i class="fs-12 fa fa-plus" aria-hidden="true"></i>\n' +
                           '    </button>\n' +
                           '</div>';
                },
            },
            {
                label: 'Всего',
                attr: 'total',
                view: 'currency'
            },
        ],
        cartStyle: 'table'
    });
    
    // настройка валюты
    simpleCart.currency({
        code: 'RU',
        symbol: ' ₽',
        name: 'Рубли',
        delimiter: ' ',
        decimal: '.',
        after: true,
        accuracy: 0
    });
    
    // по готовлности шаманим в корзине
    simpleCart.bind('ready', function () {
        if(simpleCart.items().length !== 0 && location.pathname === '/cart') {
            $('.cart .container').toggleClass('d-none');
        }
    });
    
    // уведомление о добавлении в корзину
    simpleCart.bind('afterAdd' , function( item ){
        swal({
            title: 'Корзина обновлена',
            text: item.get('name') + ' (x' + item.get('quantity') + ') теперь в корзине.',
            icon: 'success',
            button: 'Хорошо',
        });
    });
    
    // удаление последнего товара
    simpleCart.bind('beforeRemove', function () {
        if(simpleCart.items().length === 1 && location.pathname === '/cart') {
            $('.cart .container').toggleClass('d-none');
        }
    });
    
    $(document)
        .on('click', '.simpleCart_items .cart-img-product', function (e) {
            let id = $(e.currentTarget).attr('data-cart-item-id'),
                item = simpleCart.find({id});
            
            if (item.length) {
                item[0].remove();
                simpleCart.update();
            }
        })
        .on('click', '.cart-item-increment', function (e) {
            let id = $(e.currentTarget).parents('[data-cart-item-id]').attr('data-cart-item-id'),
                item = simpleCart.find({id});
        
            if (item.length) {
                item[0].increment();
                simpleCart.update();
            }
        })
        .on('click', '.cart-item-decrement', function (e) {
            let id = $(e.currentTarget).parents('[data-cart-item-id]').attr('data-cart-item-id'),
                item = simpleCart.find({id});
        
            if (item.length) {
                item[0].decrement();
                simpleCart.update();
            }
        });
    
    // обработка отправки корзины
    simpleCart.bind('beforeCheckout', function (data) {
        
        // все поля имеющие имя добавляются в набор
        $('input[name]').each((i, el) => {
            let $el = $(el);
            
            if ($el.attr('required') && !$el.val()) {
                $el
                    .one('change', () => {
                        $el.parent().removeAttr('style');
                    })
                    .parent()
                    .css({'border-color': 'red'});
                
                throw new Error('Обязательное поле ');
            }
            
            data[$el.attr('name')] = $el.val();
        });
    });
    
    // default empty query list
    location.query = {};
    
    // build query params
    window.location.href.replace(/[?&]+([^=&]+)=([^&]*)/gi, (m, key, value) => {
        location.query[key] = value;
    });
    
    /*[ Pagination Catalog ]
    ===========================================================*/
    if (location.pathname.indexOf('/catalog') >= 0) {
        $('.pagination a').on('click', (e) => {
            e.preventDefault();
            
            let $el = $(e.currentTarget);
            
            if (!$el.hasClass('active-pagination')) {
                $.ajax({
                    type: 'GET',
                    url: $el.attr('href'),
                    beforeSend: function(request) {
                        request.setRequestHeader('Particle-Content', 'pagination');
                    }
                }).done((res) => {
                    $('[data-catalog]').append(res);
                    $el.addClass('active-pagination');
                });
            }
        });
    }
    
})(jQuery);
