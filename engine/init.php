<?php declare(strict_types=1);

require_once(__DIR__ . '/libs/autoload.php');

use Doctrine\ORM\EntityManager;
use Doctrine\ORM\Tools\Setup;
use Monolog\Formatter\LineFormatter;
use Monolog\Handler\FirePHPHandler;
use Monolog\Handler\StreamHandler;
use Monolog\Logger;
use Phive\Twig\Extensions\Deferred\DeferredExtension;
use Support\Storage;

$config = [
    'debug' => false,
    'base.dir' => __DIR__ . '/..',
];

// check debug mode
if (isset($_COOKIE['debug']) && $_COOKIE['debug'] == 'orchid') {
    $config['debug'] = true;
    ini_set('display_errors', '1');
    ini_set('html_errors', '1');
    ini_set('error_reporting', '30719');
} else {
    ini_set('display_errors', '1');
    ini_set('html_errors', '1');
    ini_set('error_reporting', '0');
}

// load app
$app = app($config);

// get container
$container = $app->getContainer();

// folders
$app->path->add('root', __DIR__ . '/..');
$app->path->add('engine', $app->path->get('root:engine'));
$app->path->add('var', $app->path->get('engine:var'));
$app->path->add('cache', $app->path->get('engine:var/cache'));
$app->path->add('log', $app->path->get('engine:var/log'));
$app->path->add('skin', $app->path->get('engine:skin'));
$app->path->add('themes', $app->path->get('root:themes'));
$app->path->add('uploads', $app->path->get('root:uploads'));

// view folder
$app->path->add('view', $app->path->get('skin:templates'));

// load from src folder
$app->add('autoload', __DIR__ . '/classes');

// init monolog
$container['logger'] = function () use ($app) {
    $path = $app->path->get('var:log') . '/process.log';

    $stream = new StreamHandler($path);
    $stream->setFormatter(new LineFormatter(
        "[%datetime%] %level_name%: %message% %context% %extra%\n",
        'd-m-Y H:i:s'
    ));

    $log = new Logger('OmniCMS [' . $_SERVER['HTTP_HOST'] . ']');
    $log->pushHandler($stream);
    $log->pushHandler(new FirePHPHandler());
    $log->pushProcessor(function ($record) use ($app) {
        $req = $app->request();

        $record['extra']['client'] = [
            'ip' => $req->getServerParam('REMOTE_ADDR'),
            'agent' => $req->getServerParam('HTTP_USER_AGENT'),
        ];

        return $record;
    });

    return $log;
};

// init orm
$container['entityManager'] = function ($c) use ($app) {
    $config = Setup::createAnnotationMetadataConfiguration($app->get('autoload'), $c->debug);
    $conn = [
        'driver' => 'pdo_sqlite',
        'path' => $app->path->get('var:') . 'db.sqlite',
    ];

    $em = EntityManager::create($conn, $config);

    if ($c->isDebug()) {
        // stack
        $stack = new \Doctrine\DBAL\Logging\DebugStack();
        $em->getConfiguration()->setSQLLogger($stack);
    }

    return $em;
};

// init storage
$container['storage'] = function () {
    return new Storage();
};

// init twig
$container['view'] = function ($c) {
    $view = new \Support\View($c->path->list('view'), [
        'debug' => $c->isDebug(),
        'auto_reload' => true,
    ]);

    $view->addExtension(new \Support\ViewExtension($c));
    $view->addExtension(new Twig_Extensions_Extension_Text());
    $view->addExtension(new Twig\Extension\StringLoaderExtension());
    $view->addExtension(new DeferredExtension());

    $env = $view->getEnvironment();

    $env->addGlobal('styles', new ArrayObject());
    $env->addGlobal('scripts', new ArrayObject());
    $env->addGlobal('_request', $_REQUEST);

    // set cache path
    if (!$c->isDebug()) {
        $env->setCache($c->path->get('var:cache'));
    }

    return $view;
};

// load modules
CMSModule::load($app);
