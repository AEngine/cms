<?php

namespace Bin;

use Interfaces\BinInterface;
use InvalidArgumentException;
use News\Collection\News as CollectionNews;
use News\Model\News as ModelNews;
use Page\Collection\Page as CollectionPage;
use Page\Model\Page as ModelPage;
use samdark\sitemap\Sitemap;

class UpdateSitemap implements BinInterface
{
    public static function exec()
    {
        $app = app();
        $baseurl = rtrim($app->storage->value('module_system', 'homepage'), '/');

        $map = new Sitemap($app->getBaseDir() . '/sitemap.xml');

        /**
         * @var ModelPage $page
         */
        foreach (CollectionPage::fetch(['status' => 'work']) as $page) {
            $path = trim($baseurl . $page->path);
            try {
                $map->addItem($path, time(), Sitemap::DAILY, 1);
            } catch (InvalidArgumentException $e) {
                $app->logger->warning('Sitemap: page with Id:' . $page->id . ' have wrong url. Skipped', ['url' => $path]);
            }
        }

        /**
         * @var ModelNews $news
         */
        foreach (CollectionNews::fetch(['status' => 'work']) as $news) {
            $path = trim($baseurl . '/' . $news->category . '/' . $news->path);
            try {
                $map->addItem($path, $news->date->getTimestamp(), Sitemap::DAILY, .5);
            } catch (InvalidArgumentException $e) {
                $app->logger->warning('Sitemap: news with Id:' . $news->id . ' have wrong url. Skipped', ['url' => $path]);
            }
        }

        $map->write();

        $app->logger->info('Sitemap: created');

        return $map;
    }
}
