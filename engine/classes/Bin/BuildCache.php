<?php

namespace Bin;

use Interfaces\BinInterface;
use RecursiveDirectoryIterator;
use RecursiveIteratorIterator;

class BuildCache implements BinInterface
{
    public static function exec()
    {
        $app = app();

        /**
         * @var \Support\View $view
         */
        $view = $app->view();
        $view->getEnvironment()->setCache($app->path->get('var:cache'));

        foreach ($view->getLoader()->getPaths() as $path) {
            foreach (
                new RecursiveIteratorIterator(
                    new RecursiveDirectoryIterator($path),
                    RecursiveIteratorIterator::LEAVES_ONLY
                ) as $file
            ) {
                /** @var \SplFileInfo $file */
                if ($file->isFile() && in_array($file->getExtension(), ['twig', 'html'])) {
                    $view->getEnvironment()->loadTemplate(str_replace($path . '/', '', $file));
                }
            }
        }

        return $view;
    }
}
