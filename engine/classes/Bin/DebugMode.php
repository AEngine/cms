<?php

namespace Bin;

use Interfaces\BinInterface;
use Reference\Date as ReferenceDate;

class DebugMode implements BinInterface
{
    public static function exec()
    {
        if (app()->isDebug()) {
            setcookie('debug', '', -1, '/');
        } else {
            setcookie('debug', 'orchid', time() + ReferenceDate::YEAR, '/');
        }
    }
}
