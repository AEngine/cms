<?php

use AEngine\Orchid\App;
use AEngine\Orchid\Exception\FileNotFoundException;
use AEngine\Orchid\Http\Request;
use AEngine\Orchid\Http\Response;
use Doctrine\DBAL\Exception\TableNotFoundException;
use Support\StorageModel;

abstract class CMSModule
{
    const PARAM_TYPE_SEPARATOR = StorageModel::TYPE_SEPARATOR;
    const PARAM_TYPE_TEXT = StorageModel::TYPE_TEXT;
    const PARAM_TYPE_TEXTAREA = StorageModel::TYPE_TEXTAREA;
    const PARAM_TYPE_SELECT = StorageModel::TYPE_SELECT;
    const PARAM_TYPE_SWITCH = StorageModel::TYPE_SWITCH;

    /**
     * Приоритет обработки модуля
     *
     * @var int
     */
    public static $priority = 0;

    /**
     * Список загруженных модулей
     *
     * @var array
     */
    protected static $loaded = [];

    /**
     * Метод для загрузки модулей
     *
     * @param App $app
     *
     * @throws FileNotFoundException
     */
    public static function load(App $app)
    {
        // load modules
        static::loadFromFolder($app, $app->path->get('engine:modules'));

        // load addons
        if ($path = $app->path->get('root:addons')) {
            static::loadFromFolder($app, $path);
        }

        // sort modules
        usort(static::$loaded, function ($a, $b) {
            return $b['priority'] - $a['priority'];
        });

        // init module
        foreach (static::$loaded as $module) {
            try {
                call_user_func([$module['class'], 'onBeforeInit'], $app);
                call_user_func([$module['class'], 'onInit'], $app);
                call_user_func([$module['class'], 'onAfterInit'], $app);
            } catch (TableNotFoundException $e) {
                // nothing
            }
        }
    }

    /**
     * Загрузщик модулей
     *
     * @param App    $app
     * @param string $folder
     *
     * @throws FileNotFoundException
     */
    protected static function loadFromFolder(App $app, $folder)
    {
        // add folder to autoload
        $app->add('autoload', $folder);

        foreach (new DirectoryIterator($folder) as $element) {
            if (!$element->isDot() && (
                    $element->isDir() ||
                    $element->isFile() && $element->getExtension() == 'php'
                )
            ) {
                $dir = $element->getRealPath();
                $name = $class = $element->getBasename('.php');

                if (!is_file($dir)) {
                    $app->path->add($class, $dir);
                    $dir = $dir . DIRECTORY_SEPARATOR . 'Module' . $class . '.php';

                    // class name with namespace
                    $class = $element->getFilename() . '\\Module' . $class;
                }

                if (file_exists($dir)) {
                    require_once($dir);
                } else {
                    throw new FileNotFoundException('Could not find specified file');
                }

                // check exists
                if (!class_exists($class)) {
                    throw new RuntimeException('Class "' . $class . '" not found');
                }

                // check parent class
                switch (true) {
                    case is_subclass_of($class, static::class):
                        static::$loaded[] = [
                            'name' => $name,
                            'class' => $class,
                            'priority' => $class::$priority,
                        ];

                        break;

                    case is_subclass_of($class, \AEngine\Orchid\Module::class):
                        $app->add('module.list', $name, $class);

                        if (method_exists($class, 'initialize')) {
                            call_user_func([$class, 'initialize'], $app);
                        }

                        break;
                }

                if (file_exists(dirname($dir) . '/templates')) {
                    $app->path->add('view', dirname($dir) . '/templates');
                }
            }
        }
    }

    /**
     * Возвращает загруженные модули
     *
     * @return array
     */
    public static function getLoaded()
    {
        return static::$loaded;
    }

    /**
     * Производит настройку перед выполнением приложения
     */
    public static function setup()
    {
        $app = app();
        $viewEnvironment = $app->view->getEnvironment();

        foreach (static::$loaded as $module) {
            // вызов функции события
            call_user_func([$module['class'], 'onBeforeSetup'], $app);

            // получение настроек загруженных модулей
            if ($settings = call_user_func([$module['class'], 'getSettings'])) {
                $app->storage->set('module_' . strtolower($module['name']), $settings);
            }

            // получение функций для TWIG
            foreach (call_user_func([$module['class'], 'getFunction']) as $function) {
                $viewEnvironment->addFunction($function);
            }

            // вызов функции события
            call_user_func([$module['class'], 'onAfterSetup'], $app);
        }

        foreach ($app->storage->get() as $key => $value) {
            if (isset($value['params'])) {
                foreach ($value['params'] as $k => $v) {
                    $viewEnvironment->addGlobal($key . '_' . $k, $v['value']);
                }
            } else {
                $viewEnvironment->addGlobal($key, $value);
            }
        }

        foreach (static::$loaded as $module) {
            // вызов функции события
            call_user_func([$module['class'], 'onBeforeRunApp'], $app);
        }

        register_shutdown_function(function () use ($app) {
            foreach (static::$loaded as $module) {
                // вызов функции события
                call_user_func([$module['class'], 'onBeforeShutdown'], $app);
            }
        });
    }

    /**
     * Вызывается ДО инициализации модуля
     *
     * @param App $app
     */
    public static function onBeforeInit(App $app)
    {
        // override this method
    }

    /**
     * Производит инициализацию модуля
     *
     * @param App $app
     */
    public static function onInit(App $app)
    {
        // override this method
    }

    /**
     * Вызывается ПОСЛЕ инициализации модуля
     *
     * @param App $app
     */
    public static function onAfterInit(App $app)
    {
        // override this method
    }

    /**
     * Вызывается ДО обработки настроек модуля
     *
     * @param App $app
     */
    public static function onBeforeSetup(App $app)
    {
        // override this method
    }

    /**
     * Вызывается ПОСЛЕ обработки настроек модуля
     *
     * @param App $app
     */
    public static function onAfterSetup(App $app)
    {
        // override this method
    }

    /**
     * Вызывается ДО начала работы приложения
     *
     * @param App $app
     */
    public static function onBeforeRunApp(App $app)
    {
        // override this method
    }

    /**
     * Вызывается ДО "смерти" сеанса PHP
     *
     * @param App $app
     */
    public static function onBeforeShutdown(App $app)
    {
        // override this method
    }

    /**
     * Добавляет группу роутеров
     *
     * @param string $pattern
     * @param string|array|Closure $callable
     *
     * @return \AEngine\Orchid\Interfaces\RouteGroupInterface
     */
    public static function addRouteGroup($pattern, $callable)
    {
        return app()->router()->group($pattern, $callable);
    }

    /**
     * Добавляет маршрут
     *
     * @param string|array         $methods
     * @param string               $pattern
     * @param string|array|Closure $callable
     * @param int                  $priority
     *
     * @return \AEngine\Orchid\Interfaces\RouteInterface
     */
    public static function addRoute($methods, $pattern, $callable, $priority = 0)
    {
        $app = app();

        if ($methods === 'any') {
            $methods = ['GET', 'POST', 'PUT', 'PATCH', 'DELETE', 'OPTIONS', 'HEAD'];
        }
        if (!is_array($methods)) {
            $methods = [$methods];
        }

        $route = $app->router()->map($methods, $pattern, $callable, $priority);

        // in middleware use user theme in twig
        $route->addMiddleware(function (Request $req, Response $res, $next) use ($app) {
            if ($path = $app->path->get('themes:' . $app->storage->value('module_system', 'theme') . '/templates')) {
                $app->view->getLoader()->addPath($path);
            }

            $res = $res->withAddedHeader('X-Powered-By', 'AEngine OmniCMS');

            return $next($req, $res);
        });

        return $route;
    }

    /**
     * Получение настроек модуля по-умолчанию
     *
     * @return mixed
     */
    public static function getSettings()
    {
        return [
            /*
            'title' => 'Мой модуль',
            'description' => 'Описание модуля',
            'params' => [ // список редактируемых параметров модуля
               [
                   'title' => 'Имя параметра',
                   'description' => 'Описание параметра',
                   'value' => 'Значение по-умолчанию',
                   'type' => 'text', // text или textarea
                   'editable' => true, // отображать параметр
               ],
            ],
            'options' => [ // список свойств модуля
               'key' => 'value',
            ],
            'editable' => true, // отображать раздел или нет в настройках
            */
        ];
    }

    /**
     * Получение функций для Twig
     */
    public static function getFunction()
    {
        return [];
    }

    /**
     * Получение скриптов модуля
     */
    public static function getBin()
    {
        return [
            /*[
                'title'  => 'Очистить кеш',
                'script' => '\MyModule\Bin\ClassName',
            ],*/
        ];
    }

    /**
     * Получение статистики модуля
     *
     * @return array
     */
    public static function getStatistic()
    {
        return [
            /*[
                'title' => 'Мой пункт статистики',
                'value' => 'Значение',
            ],*/
        ];
    }
}

/**
 * Функция генерирует содержимое
 *
 * @param Request  $req
 * @param Response $res
 * @param          $tpl
 * @param array    $data
 *
 * @return mixed
 */
function render(Request $req, Response $res, $tpl, array $data = [])
{
    $app = app();

    if ($req->isXhr() && $req->hasHeader('Particle-Content')) {
        $block = ucfirst(
            strtolower(
                $req->getHeaderLine('Particle-Content')
            )
        );

        return $res
            ->withAddedHeader('Cache-Control', 'no-cache, no-store, must-revalidate')
            ->withAddedHeader('Pragma', 'no-cache')
            ->withAddedHeader('Expires', '0')
            ->write(
                $app->view->fetchBlock($tpl, 'Particle' . $block, $data)
            );
    }

    return $app->view->render($res, $tpl, $data);
}
