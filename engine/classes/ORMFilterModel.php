<?php

use AEngine\Orchid\Filter\FilterModel;

class ORMFilterModel extends FilterModel
{
    /**
     * @param array      $criteria
     * @param array|null $orderBy
     *
     * @return static|null
     */
    public static function fetch(array $criteria = [], array $orderBy = null)
    {
        /**
         * @var \Doctrine\ORM\EntityManager $em
         */
        $em = app()->entityManager;

        return $em->getRepository(static::class)->findOneBy($criteria, $orderBy);
    }

    /**
     * @return $this
     * @throws \Doctrine\ORM\OptimisticLockException
     * @throws \Doctrine\ORM\ORMException
     */
    public function save()
    {
        /**
         * @var \Doctrine\ORM\EntityManager $em
         */
        $em = app()->entityManager;

        if (!$em->isOpen()) {
            $em = $em->create($em->getConnection(), $em->getConfiguration());
        }

        $em->persist($this);
        $em->flush();

        return $this;
    }

    /**
     * @return FilterModel|void
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function remove()
    {
        /**
         * @var \Doctrine\ORM\EntityManager $em
         */
        $em = app()->entityManager;

        if (!$em->isOpen()) {
            $em = $em->create($em->getConnection(), $em->getConfiguration());
        }

        $em->remove($this);
        $em->flush();
    }
}
