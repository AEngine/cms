<?php

namespace Support;

use AEngine\Orchid\Filter as Filter;
use Doctrine\ORM\Mapping\Cache;
use Doctrine\ORM\Mapping\Column;
use Doctrine\ORM\Mapping\Entity;
use Doctrine\ORM\Mapping\GeneratedValue;
use Doctrine\ORM\Mapping\Id;
use Doctrine\ORM\Mapping\Table;
use ORMFilterModel;

/**
 * @Entity
 * @Table(name="Storage")
 * @Cache()
 */
class StorageModel extends ORMFilterModel
{
    const TYPE_SEPARATOR = 'separator';
    const TYPE_TEXT = 'text';
    const TYPE_TEXTAREA = 'textarea';
    const TYPE_SELECT = 'select';
    const TYPE_SWITCH = 'switch';

    /**
     * @Id
     * @Column(type="integer")
     * @GeneratedValue
     * @Filter\Lead\Integer()
     *
     * @var int
     */
    public $id;

    /**
     * @Column(type="string")
     * @Filter\Required()
     * @Filter\Lead\Str()
     * @Filter\Check\ValueNotEmpty(message="Поле не может быть пустым")
     *
     * @var string
     */
    public $namespace;

    /**
     * @Column(type="string")
     * @Filter\Required()
     * @Filter\Lead\Str()
     * @Filter\Check\ValueNotEmpty(message="Поле не может быть пустым")
     *
     * @var array
     */
    public $key;

    /**
     * @Column(type="string")
     * @Filter\Required()
     * @Filter\Lead\Str()
     *
     * @var string
     */
    public $value;
}
