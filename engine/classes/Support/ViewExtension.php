<?php

namespace Support;

class ViewExtension extends \Twig_Extension
{
    /**
     * @var \AEngine\Orchid\Container
     */
    protected $container;

    public function __construct($container)
    {
        $this->container = $container;
    }

    public function getName()
    {
        return 'OmniCMS';
    }

    public function getFunctions()
    {
        $c = $this->container;
        $uri = \AEngine\Orchid\Http\Uri::createFromGlobals(
            \AEngine\Orchid\Http\Environment::mock($_SERVER)
        );

        return [
            new \Twig\TwigFunction('base_url', function () use ($uri) {
                if (method_exists($uri, 'getBaseUrl')) {
                    return $uri->getBaseUrl();
                }

                return '/';
            }),
            new \Twig\TwigFunction('is_current_path', function ($path) use ($uri) {
                return $uri->getBaseUrl() . '/' . ltrim($path, '/') === $uri->getBaseUrl() . '/' . ltrim($uri->getPath(), '/');
            }),
            new \Twig\TwigFunction('current_path', function ($withBasePath = true, $withQueryString = false) use ($uri) {
                $path = '/' . ltrim($uri->getPath(), '/');

                if ($withBasePath) {
                    $path = $uri->getBaseUrl() . $path;
                }

                if ($withQueryString && '' !== $query = $uri->getQuery()) {
                    $path .= '?' . $query;
                }

                return $path;
            }),
            new \Twig\TwigFunction('current_query', function () use ($uri) {
                return '?' . (explode('?', $_SERVER['REQUEST_URI'] ?? '?')[1] ?? '');
            }),
            new \Twig\TwigFunction('similar_path', function ($path, $prefix = false) use ($uri) {
                $currentPath = '/' . ltrim($uri->getPath(), '/');

                if ($prefix) {
                    $currentPath = str_replace($prefix, '', $currentPath);
                    $path = str_replace($prefix, '', $path);
                }

                if ($path) {
                    return similar_text($path, $currentPath);
                }

                return false;
            }),
            new \Twig\TwigFunction('pre', function (...$args) use ($uri, $c) {
                if ($c->isDebug()) {
                    call_user_func_array('pre', $args);
                }
            }),
            new \Twig\TwigFunction('collect', function (array $array) use ($uri, $c) {
                return collect($array);
            }),
        ];
    }
}
