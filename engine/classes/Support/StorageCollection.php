<?php

namespace Support;

use ORMCollection;

class StorageCollection extends ORMCollection
{
    public static $model = StorageModel::class;

    public static function fetch(array $criteria = [], array $orderBy = null, $limit = null, $offset = null)
    {
        $collection = parent::fetch($criteria, $orderBy, $limit, $offset);
        $collection = $collection->mapWithKeys(function ($item) {
            return [$item->namespace . '_' . $item->key => $item];
        });

        return $collection;
    }
}
