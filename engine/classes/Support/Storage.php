<?php

namespace Support;

use AEngine\Orchid\Collection;

class Storage
{
    /**
     * @var Collection
     */
    protected $internal;

    /**
     * @var Collection
     */
    protected $stored;

    public function __construct()
    {
        $this->internal = collect();
        $this->stored = StorageCollection::fetch();
    }

    public function set($id, array $values = [])
    {
        // если начинается с module_ значит - настройки модуля
        if (str_starts_with('module_', $id)) {
            $default = [
                'title' => '',
                'description' => '',
                'params' => [],
                'options' => [],
                'editable' => true,
            ];
            $defaultParam = [
                'id' => null,
                'title' => '',
                'description' => '',
                'placeholder' => '',
                'value' => '',
                'editable' => true,
                'type' => StorageModel::TYPE_TEXT,
                'options' => [],
                'html' => '',
            ];

            $values = array_merge($default, $values);

            foreach ($values['params'] as $key => &$value) {
                $value = array_merge($defaultParam, $value);

                // если есть в хранилище меняем значение на установленное
                if ($this->stored->has($id . '_' . $key)) {
                    $model = $this->stored->get($id . '_' . $key);

                    $values['params'][$key]['id'] = $model->id;
                    $values['params'][$key]['value'] = $model->value;
                }
            }
        }

        return $this->internal[$id] = $values;
    }

    public function add($id, ...$element)
    {
        $buf = (array)$this->get($id);

        switch (count($element)) {
            case 1:
                $buf[] = $element[0];
                break;
            case 2:
                $buf[$element[0]] = $element[1];
                break;
        }

        static::set($id, $buf);
    }

    public function get($id = null, $default = [])
    {
        if (!is_null($id)) {
            return $this->internal[$id] ?? $default;
        }

        return $this->internal;
    }

    public function value($id, $key, $default = [])
    {
        return $this->internal[$id]['params'][$key]['value'] ?? $default;
    }

    /**
     * Для изменения полей
     *
     * @param string $namespace
     * @param array  $data
     *
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function change($namespace, $data)
    {
        $params = [];

        // текущие значения
        $current = $this->internal->get($namespace);
        if (!isset($current['params'])) {
            return;
        }
        $current = $current['params'];

        // выбираем поля которые можно менять
        foreach ($data as $key => $value) {
            if (in_array($key, array_keys($current))) {
                $params[$key] = $value;
            }
        }

        // все устраивает идем дальше
        if ($namespace && $params) {
            $changed = [];

            foreach ($current as $key => $value) {
                if ($value['type'] != 'separator') {
                    if ($value['value'] != $params[$key]) {
                        $changed[$key] = $params[$key];
                    }
                }
            }

            $logger = app()->logger;

            foreach ($changed as $key => $value) {
                if ($current[$key]['id']) {
                    $item = StorageModel::fetch(['id' => $current[$key]['id']]);
                } else {
                    $item = new StorageModel();
                }

                // значение пустое, удалем замену из таблицы и пропускаем
                if (blank($value)) {
                    if ($current[$key]['id']) {
                        $item->remove();
                    }
                    $logger->info('App settings will be returned to default', ['namespace' => $namespace, 'key' => $key, 'before' => $item->value]);

                    continue;
                }

                // пишем в модель новые данные
                $item->replace([
                    'namespace' => $namespace,
                    'key' => $key,
                    'value' => $value,
                ]);

                // фильтруем и сохраняем
                if ($item->filter() === true) {
                    $item->save();
                    $logger->info('App settings will be changed', ['namespace' => $namespace, 'key' => $key, 'before' => $current[$key]['value'] ?? '', 'after' => $value]);
                }
            }
        }
    }
}

