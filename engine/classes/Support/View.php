<?php

namespace Support;

use ArrayAccess;
use ArrayIterator;
use Psr\Http\Message\ResponseInterface;
use Throwable;
use Twig\Environment as Twig_Environment;
use Twig_Error_Loader;
use Twig_Error_Runtime;
use Twig_Error_Syntax;
use Twig_ExtensionInterface;
use Twig\Loader\FilesystemLoader as Twig_Loader_Filesystem;
use Twig_LoaderInterface;

class View implements ArrayAccess
{
    /**
     * Twig loader
     *
     * @var Twig_LoaderInterface
     */
    protected $loader;

    /**
     * Twig environment
     *
     * @var Twig_Environment
     */
    protected $environment;

    /**
     * Default view variables
     *
     * @var array
     */
    protected $defaultVariables = [];

    /**
     * Create new Twig view
     *
     * @param string|array $paths    Path(s) to templates directory
     * @param array        $settings Twig environment settings
     *
     * @throws Twig_Error_Loader
     */
    public function __construct($paths, $settings = [])
    {
        $this->loader = new Twig_Loader_Filesystem();

        foreach ((array)$paths as $namespace => $path) {
            if (is_string($namespace)) {
                $this->loader->setPaths($path, $namespace);
            } else {
                $this->loader->addPath($path);
            }
        }

        $this->environment = new Twig_Environment($this->loader, $settings);
    }

    /**
     * Fetch rendered template
     *
     * @param string $template Template pathname relative to templates directory
     * @param array  $data     Associative array of template variables
     *
     * @return string
     * @throws Twig_Error_Loader
     * @throws Twig_Error_Runtime
     * @throws Twig_Error_Syntax
     */
    public function fetch($template, $data = [])
    {
        $data = array_merge($this->defaultVariables, $data);

        return $this->environment->render($template, $data);
    }

    /**
     * Fetch rendered block
     *
     * @param string $template Template pathname relative to templates directory
     * @param string $block    Name of the block within the template
     * @param array  $data     Associative array of template variables
     *
     * @return string
     * @throws Twig_Error_Loader
     * @throws Twig_Error_Syntax
     * @throws Twig_Error_Runtime
     */
    public function fetchBlock($template, $block, $data = [])
    {
        $data = array_merge($this->defaultVariables, $data);

        return $this->environment->loadTemplate($template)->renderBlock($block, $data);
    }

    /**
     * Fetch rendered string
     *
     * @param string $string String
     * @param array  $data   Associative array of template variables
     *
     * @return string
     * @throws Twig_Error_Loader
     * @throws Throwable
     * @throws Twig_Error_Syntax
     */
    public function fetchFromString($string = "", $data = [])
    {
        $data = array_merge($this->defaultVariables, $data);

        return $this->environment->createTemplate($string)->render($data);
    }

    /**
     * Output rendered template
     *
     * @param ResponseInterface $response
     * @param string            $template Template pathname relative to templates directory
     * @param array             $data     Associative array of template variables
     *
     * @return ResponseInterface
     * @throws Twig_Error_Loader
     * @throws Twig_Error_Runtime
     * @throws Twig_Error_Syntax
     */
    public function render(ResponseInterface $response, $template, $data = [])
    {
        $response->getBody()->write($this->fetch($template, $data));

        return $response;
    }

    /**
     * Proxy method to add an extension to the Twig environment
     *
     * @param Twig_ExtensionInterface $extension A single extension instance or an array of instances
     */
    public function addExtension(Twig_ExtensionInterface $extension)
    {
        $this->environment->addExtension($extension);
    }

    /**
     * Return Twig loader
     *
     * @return Twig_LoaderInterface
     */
    public function getLoader()
    {
        return $this->loader;
    }

    /**
     * Return Twig environment
     *
     * @return Twig_Environment
     */
    public function getEnvironment()
    {
        return $this->environment;
    }

    /**
     * Does this collection have a given key?
     *
     * @param string $key The data key
     *
     * @return bool
     */
    public function offsetExists($key)
    {
        return array_key_exists($key, $this->defaultVariables);
    }

    /**
     * Get collection item for key
     *
     * @param string $key The data key
     *
     * @return mixed The key's value, or the default value
     */
    public function offsetGet($key)
    {
        return $this->defaultVariables[$key];
    }

    /**
     * Set collection item
     *
     * @param string $key   The data key
     * @param mixed  $value The data value
     */
    public function offsetSet($key, $value)
    {
        $this->defaultVariables[$key] = $value;
    }

    /**
     * Remove item from collection
     *
     * @param string $key The data key
     */
    public function offsetUnset($key)
    {
        unset($this->defaultVariables[$key]);
    }

    /**
     * Get number of items in collection
     *
     * @return int
     */
    public function count()
    {
        return count($this->defaultVariables);
    }

    /**
     * Get collection iterator
     *
     * @return ArrayIterator
     */
    public function getIterator()
    {
        return new ArrayIterator($this->defaultVariables);
    }
}
