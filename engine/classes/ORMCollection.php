<?php

use AEngine\Orchid\Collection;

class ORMCollection extends Collection
{
    // модель коллекции
    public static $model;

    // критерий выборки
    public $_criteria;

    // сортировка
    public $_orderBy;

    // ограничение
    public $_limit;

    // отступ
    public $_offset;

    // всего записей
    public $_rowsCount;

    /**
     * @param array $criteria
     * @param array $orderBy
     * @param mixed $limit
     * @param mixed $offset
     *
     * @return ORMCollection
     */
    public static function fetch(array $criteria = [], array $orderBy = null, $limit = null, $offset = null)
    {
        /**
         * @var \Doctrine\ORM\EntityManager $em
         */
        $em = app()->entityManager;
        $result = $em->getRepository(static::$model ? static::$model : static::class)->findBy($criteria, $orderBy, $limit, $offset);

        $array = [];
        foreach ($result as $key => $value) {
            $array[property_exists($value, 'id') ? $value->id : $key] = $value;
        }

        $collection = new static($array);
        $collection->_criteria = $criteria;
        $collection->_orderBy = $orderBy;
        $collection->_limit = (int)$limit;
        $collection->_offset = (int)$offset;

        return $collection;
    }

    /**
     * @return $this
     */
    public function calcRowsCount()
    {
        if (!$this->_rowsCount) {
            /**
             * @var \Doctrine\ORM\EntityManager $em
             */
            $em = app()->entityManager;
            $this->_rowsCount= count($em->getRepository(static::$model ? static::$model : static::class)->findBy($this->_criteria, $this->_orderBy));
        }

        return $this;
    }

    /**
     * @return int
     */
    public function getRowsCount()
    {
        if (!$this->_rowsCount) {
            $this->calcRowsCount();
        }

        return $this->_rowsCount;
    }

    /**
     * Возвращает массив для Form::select
     *
     * @param string $field
     *
     * @return array
     */
    public function toSelect($field = 'name')
    {
        $result = [];

        foreach ($this->data as $key => $value) {
            $result[$value['id'] ?? $key] = is_array($value) && isset($value[$field]) ? $value[$field] : $value;
        }

        return $result;
    }
}
