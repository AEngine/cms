<?php

namespace Page\Collection;

use ORMCollection;
use Doctrine\ORM\Mapping\Cache;

/**
 * @Cache()
 */
class Page extends ORMCollection
{
    public static $model = \Page\Model\Page::class;
}

