<?php

namespace Page;

use AEngine\Orchid\App;
use AEngine\Orchid\Http\Request;
use AEngine\Orchid\Http\Response;
use CMSModule;
use Page\Collection\Page as CollectionPage;
use Page\Model\Page as ModelPage;

class ModulePage extends CMSModule
{
    public static $priority = 100;

    /**
     * @param App $app
     */
    public static function onInit(App $app)
    {
        // публичная страница
        static::addRoute('get', '*', function (Request $req, Response $res) {
            $page = ModelPage::fetch([
                'path' => $req->getUri()->getPath(),
                'status' => 'work',
            ]);
            $status = 404;
            $template = ['{% extends ["p404.html", "p404.twig", "cup/p404.html", "cup/p404.twig"] %}'];

            if ($page) {
                $status = 200;
                $template = [];

                if ($page->template) {
                    $template[] = '{% extends "' . $page->template . '" %}';

                    if ($page->content && $page->content != '<p><br></p>') { // костыль
                        $template[] = '{% block content %}';
                        $template[] = $page->content;
                        $template[] = '{% endblock %}';
                    }
                } else {
                    $template[] = $page->content;
                }
            }

            return $res->withStatus($status)->write(
                $this->view->fetchFromString(implode(PHP_EOL, $template), ['page' => $page])
            );
        }, -100);

        if (panel_is_admin() === true) {
            // админ панель
            static::addRouteGroup('/cup/page', function () {
                // список страниц
                static::addRoute('get', '', function (Request $req, Response $res) {
                    return render($req, $res, 'page/index.html', [
                        'list' => CollectionPage::fetch(['status' => 'work'], ['path' => 'asc']),
                    ]);
                });

                // добавление страницы
                static::addRoute(['get', 'post'], '/add', function (Request $req, Response $res) {
                    if ($req->isPost()) {
                        $item = new ModelPage([
                            'title' => $req->getParam('title', ''),
                            'path' => $req->getParam('path', ''),
                            'description' => $req->getParam('description', ''),
                            'template' => $req->getParam('template', 0),
                            'content' => $req->getParam('content', ''),
                        ]);
                        $check = $item->filter();

                        if ($check === true) {
                            $item->save();
                            $this->logger->info('Page has been added', ['id' => $item->id]);

                            return $res->withAddedHeader('Location', '/cup/page');
                        }
                        $this->view->getEnvironment()->addGlobal('_error', $check);
                    }

                    return render($req, $res, 'page/form.html', ['_post' => $_POST]);
                });

                // редактирование страницы
                static::addRoute(['get', 'post'], '/:id/edit', function (Request $req, Response $res, $args) {
                    $item = ModelPage::fetch($args + ['status' => 'work']);

                    if ($item) {
                        if ($req->isPost()) {
                            $item->replace([
                                'title' => $req->getParam('title', ''),
                                'path' => $req->getParam('path', ''),
                                'description' => $req->getParam('description', ''),
                                'template' => $req->getParam('template', 0),
                                'content' => $req->getParam('content', ''),
                            ]);
                            $check = $item->filter();

                            if ($check === true) {
                                $item->save();
                                $this->logger->info('Page has been edited', ['id' => $item->id]);

                                return $res->withAddedHeader('Location', '/cup/page');
                            }
                            $this->view->getEnvironment()->addGlobal('_error', $check);
                        } else {
                            // для отрисовки шаблона
                            $_POST = $item->toArray();
                        }

                        return render($req, $res, 'page/form.html', ['_post' => $_POST]);
                    }

                    return $res->withAddedHeader('Location', '/cup/page');
                });

                // удаление страницы
                static::addRoute(['get', 'post'], '/:id/delete', function (Request $req, Response $res, $args) {
                    $item = ModelPage::fetch($args + ['status' => 'work']);

                    if ($item) {
                        $item->set('status', 'delete')->save();
                        $this->logger->info('Page has been mark as deleted', ['id' => $item->id]);
                    }

                    return $res->withAddedHeader('Location', '/cup/page');
                });
            });

            // пункт в меню
            panel_navbar_add([
                'title' => 'Страницы',
                'icon' => 'fa-file',
                'link' => '/page',
                'items' => [
                    [
                        'title' => 'Список',
                    ],
                ],
                'position' => 1,
            ]);
        }
    }

    // настройки модуля
    public static function getSettings()
    {
        return [
            'title' => 'Страницы',
            'description' => 'Позволяет добавлять/редактировать/просматривать страницы сайта',
            'editable' => false,
        ];
    }

    // обработка статистики
    public static function getStatistic()
    {
        return [
            [
                'title' => 'Страниц',
                'value' => CollectionPage::fetch(['status' => 'work'])->count(),
            ]
        ];
    }
}
