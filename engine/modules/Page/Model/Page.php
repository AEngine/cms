<?php

namespace Page\Model;

use AEngine\Orchid\Support\Str;
use Doctrine\ORM\Mapping\Cache;
use Doctrine\ORM\Mapping\Column;
use Doctrine\ORM\Mapping\Entity;
use Doctrine\ORM\Mapping\GeneratedValue;
use Doctrine\ORM\Mapping\Id;
use Doctrine\ORM\Mapping\Table;
use ORMFilterModel;
use AEngine\Orchid\Filter as Filter;

/**
 * @Entity
 * @Table(name="Pages")
 * @Cache()
 */
class Page extends ORMFilterModel
{
    /**
     * @Id
     * @Column(type="integer")
     * @GeneratedValue
     * @Filter\Lead\Integer();
     *
     * @var int
     */
    public $id;

    /**
     * @Column(type="string")
     * @Filter\Required();
     * @Filter\Lead\Str();
     * @Filter\Check\ValueNotEmpty(message="Поле не может быть пустым");
     *
     * @var string
     */
    public $title;

    /**
     * @Column(type="string")
     * @Filter\Lead\Str();
     *
     * @var string
     */
    public $description;

    /**
     * @Column(type="string")
     * @Filter\Required();
     * @Filter\Lead\Str();
     * @Filter\Check\ValueNotEmpty(message="Поле не может быть пустым");
     *
     * @var string
     */
    public $path;

    /**
     * @Column(type="string")
     * @Filter\Lead\Str();
     *
     * @var string
     */
    public $template;

    /**
     * @Column(type="text")
     * @Filter\Lead\Str();
     *
     * @var string
     */
    public $content = '';

    /**
     * @Column(type="string")
     * @Filter\Lead\Str();
     *
     * @var string
     */
    public $status = 'work';

    public function replace(array $data)
    {
        parent::replace($data);

        $this->path = str_replace('--', '', preg_replace('/[^a-z0-9\/]/ui', '-', strtolower(Str::translate(trim($this->path)))));

        return $this;
    }
}
