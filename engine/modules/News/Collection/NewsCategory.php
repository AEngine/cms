<?php

namespace News\Collection;

use Doctrine\ORM\Mapping\Cache;
use ORMCollection;

/**
 * @Cache()
 */
class NewsCategory extends ORMCollection
{
    public static $model = \News\Model\NewsCategory::class;
}

