<?php

namespace News\Collection;

use Doctrine\ORM\Mapping\Cache;
use ORMCollection;

/**
 * @Cache()
 */
class News extends ORMCollection
{
    public static $model = \News\Model\News::class;
}

