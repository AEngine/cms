<?php

namespace News;

use AEngine\Orchid\App;
use AEngine\Orchid\Http\Request;
use AEngine\Orchid\Http\Response;
use CMSModule;
use News\Collection\News as CollectionNews;
use News\Collection\NewsCategory as CollectionNewsCategory;
use News\Model\News as ModelNews;
use News\Model\NewsCategory as ModelNewsCategory;
use Twig\TwigFunction as Twig_SimpleFunction;
use Upload\Collection\File as CollectionFile;
use Upload\Model\File as ModelFile;

class ModuleNews extends CMSModule
{
    public static $priority = 100;

    /**
     * @param App $app
     */
    public static function onInit(App $app)
    {
        // готовим маску
        $mask = CollectionNewsCategory::fetch(['status' => 'work'])
            ->pluck('path')
            ->map(function ($v) {
                return str_replace('/', '\/', $v);
            })
            ->implode('|');

        /*
         * /news
         * /news/44
         * /news/title
         */
        static::addRoute('get', "#(?<category>{$mask})\/?(?<arg>[a-z0-9\-]*)#", function (Request $req, Response $res, $args) {
            $default = [
                'category' => '',
                'offset' => 0,
                'row' => '',
                'arg' => '',
            ];
            $data = array_merge($default, $args[':capture']);

            if (is_numeric($data['arg'])) {
                $data['offset'] = (int)$data['arg'];
            } else {
                $data['row'] = $data['arg'];
            }

            if ($data['row']) {
                $item = ModelNews::fetch(['path' => $data['row'], 'status' => 'work']);

                if ($item) {
                    return render($req, $res, 'news/item.html', ['item' => $item]);
                }

                return $res->withAddedHeader('Location', '/news');
            }

            return render($req, $res, 'news/list.html', [
                'category' => $data['category'],
                'limit' => $this->storage->value('module_news', 'pagination'),
                'offset' => $data['offset'],
            ]);
        }, -10);

        if (panel_is_admin() === true) {
            // админ панель
            static::addRouteGroup('/cup/news', function () {
                // список новостей
                static::addRoute('get', '', function (Request $req, Response $res) {
                    $filter = [
                        'status' => ['work', 'archive'],
                    ];
                    if ($category = $req->getParam('category', false)) {
                        if (is_numeric($category)) {
                            $buf = ModelNewsCategory::fetch(['id' => $category]);
                            $category = $buf->path;
                        }

                        $filter['category'] = $category;
                    }

                    return render($req, $res, 'news/index.html', [
                        'list' => CollectionNews::fetch($filter, [$this->storage->value('module_news', 'orderBy', 'id') => 'desc']),
                        'category' => CollectionNewsCategory::fetch(['status' => 'work'], ['position' => 'asc']),
                    ]);
                });

                // добавление новости
                static::addRoute(['get', 'post'], '/add', function (Request $req, Response $res) {
                    $category = CollectionNewsCategory::fetch(['status' => 'work'], ['position' => 'asc']);

                    if ($req->isPost()) {
                        $item = new ModelNews([
                            'title' => $req->getParam('title', ''),
                            'description' => $req->getParam('description', ''),
                            'content' => $req->getParam('content', ''),
                            'date' => $req->getParam('date', ''),
                            'category' => $req->getParam('category', ''),
                        ]);

                        $check = $item->filter();

                        if ($check === true) {
                            $item->save();

                            if (($files = $req->getParam('file', false)) !== false) {
                                $files = CollectionFile::fetch(['id' => $files]);

                                /** @var ModelFile $file */
                                foreach ($files as $file) {
                                    $file->item = 'news';
                                    $file->item_id = $item->id;

                                    if (str_ends_with(['jpg', 'jpeg', 'png', 'gif'], $file->name)) {
                                        $file->convert();
                                    }

                                    $file->save();
                                }
                            }

                            $this->logger->info('News has been added', ['id' => $item->id]);

                            return $res->withAddedHeader('Location', '/cup/news');
                        }
                        $this->view->getEnvironment()->addGlobal('_error', $check);
                    }

                    return render($req, $res, 'news/form.html', ['_post' => $_POST, 'category' => $category]);
                });

                // редактирование новости
                static::addRoute(['get', 'post'], '/:id/edit', function (Request $req, Response $res, $args) {
                    $item = ModelNews::fetch($args + ['status' => ['work', 'archive']]);

                    if ($item) {
                        $category = CollectionNewsCategory::fetch(['status' => 'work'], ['position' => 'asc']);

                        if ($req->isPost()) {
                            $item->replace([
                                'title' => $req->getParam('title', ''),
                                'description' => $req->getParam('description', ''),
                                'content' => $req->getParam('content', ''),
                                'date' => $req->getParam('date', ''),
                                'category' => $req->getParam('category', ''),
                            ]);

                            if ($req->getParam('archive', false) !== false) {
                                $item->set('status', 'archive');
                            } else {
                                if ($item->get('status') === 'archive' && $req->getParam('archive', false) === false) {
                                    $item->set('status', 'work');
                                }
                            }

                            $check = $item->filter();

                            if ($check === true) {
                                $item->save();

                                if (($files = $req->getParam('file', false)) !== false) {
                                    $files = CollectionFile::fetch(['id' => $files]);

                                    /** @var ModelFile $file */
                                    foreach ($files as $file) {
                                        $file->item = 'news';
                                        $file->item_id = $item->id;

                                        if (str_ends_with(['jpg', 'jpeg', 'png', 'gif'], $file->name)) {
                                            $file->convert();
                                        }

                                        $file->save();
                                    }
                                }

                                $this->logger->info('News has been edited', ['id' => $item->id]);

                                return $res->withAddedHeader('Location', '/cup/news');
                            }
                            $this->view->getEnvironment()->addGlobal('_error', $check);
                        } else {
                            // для отрисовки шаблона
                            $_POST = $item->toArray();
                        }

                        return render($req, $res, 'news/form.html', ['_post' => $_POST, 'files' => $item->getFiles(), 'category' => $category]);
                    }

                    return $res->withAddedHeader('Location', '/cup/news');
                });

                // удаление новости
                static::addRoute(['get', 'post'], '/:id/delete', function (Request $req, Response $res, $args) {
                    $item = ModelNews::fetch($args + ['status' => ['work', 'archive']]);

                    if ($item) {
                        $item->set('status', 'delete')->save();
                        $this->logger->info('News has been mark as deleted', ['id' => $item->id]);
                    }

                    return $res->withAddedHeader('Location', '/cup/news');
                });

                // дублирование новости
                static::addRoute(['get', 'post'], '/:id/duplicate', function (Request $req, Response $res, $args) {
                    $item = ModelNews::fetch($args + ['status' => ['work', 'archive']]);

                    if ($item) {
                        $item = new ModelNews($item->toArray() + ['id' => null]);
                        $item->save();
                    }

                    return $res->withAddedHeader('Location', '/cup/news');
                });

                // категории
                static::addRouteGroup('/category', function () {
                    // список категорий
                    static::addRoute('get', '', function (Request $req, Response $res, $args) {
                        return render($req, $res, 'news/category/index.html', [
                            'list' => CollectionNewsCategory::fetch(['status' => 'work'], ['position' => 'asc']),
                        ]);
                    });

                    // добавление категории
                    static::addRoute(['get', 'post'], '/add', function (Request $req, Response $res, $args) {
                        if ($req->isPost()) {
                            $item = new ModelNewsCategory([
                                'title' => $req->getParam('title', ''),
                                'description' => $req->getParam('description', ''),
                                'path' => $req->getParam('path', ''),
                                'slider' => $req->getParam('slider', false),
                            ]);
                            $check = $item->filter();

                            if ($check === true) {
                                $item->save();
                                $this->logger->info('Category has been added', ['id' => $item->id]);

                                return $res->withAddedHeader('Location', '/cup/news/category');
                            }
                            $this->view->getEnvironment()->addGlobal('_error', $check);
                        }

                        return render($req, $res, 'news/category/form.html', ['_post' => $_POST]);
                    });

                    // редактирование категории
                    static::addRoute(['get', 'post'], '/:id/edit', function (Request $req, Response $res, $args) {
                        $item = ModelNewsCategory::fetch($args + ['status' => 'work']);

                        if ($item) {
                            if ($req->isPost()) {
                                $item->replace([
                                    'title' => $req->getParam('title', ''),
                                    'description' => $req->getParam('description', ''),
                                    'path' => $req->getParam('path', ''),
                                    'slider' => $req->getParam('slider', false),
                                ]);
                                $check = $item->filter();

                                if ($check === true) {
                                    $item->save();
                                    $this->logger->info('Category has been edited', ['id' => $item->id]);

                                    return $res->withAddedHeader('Location', '/cup/news/category');
                                }
                                $this->view->getEnvironment()->addGlobal('_error', $check);
                            } else {
                                // для отрисовки шаблона
                                $_POST = $item->toArray();
                            }

                            return render($req, $res, 'news/category/form.html', ['_post' => $_POST]);
                        }

                        return $res->withAddedHeader('Location', '/cup/news/category');
                    });

                    // удаление категории
                    static::addRoute(['get', 'post'], '/:id/delete', function (Request $req, Response $res, $args) {
                        $item = ModelNewsCategory::fetch($args + ['status' => 'work']);

                        if ($item) {
                            $item->set('status', 'delete')->save();
                            $this->logger->info('Category has been mark as deleted', ['id' => $item->id]);
                        }

                        return $res->withAddedHeader('Location', '/cup/news/category');
                    });
                });
            });

            // пункт в меню
            panel_navbar_add([
                'title' => 'Записи',
                'icon' => 'fa-newspaper-o',
                'link' => '/news',
                'items' => [
                    [
                        'title' => 'Список',
                    ],
                    [
                        'title' => 'Категории',
                        'link' => '/category',
                    ],
                ],
                'position' => 2,
            ]);
        }
    }

    // настройки модуля
    public static function getSettings()
    {
        return [
            'title' => 'Записи',
            'description' => 'Позволяет добавлять/редактировать/просматривать записи',
            'params' => [
                'orderBy' => [
                    'title' => 'Сортировка записей',
                    'description' => 'Порядок сортировки записей',
                    'type' => static::PARAM_TYPE_SELECT,
                    'options' => [
                        'id' => 'По порядку',
                        'date' => 'По дате',
                    ],
                    'value' => 'id',
                ],
                'pagination' => [
                    'title' => 'На страницу',
                    'description' => 'Количество записей',
                    'value' => 10,
                ],
            ],
        ];
    }

    public static function getFunction()
    {
        return [
            new Twig_SimpleFunction('news_fetch', function ($category, $limit = 10, $offset = null) {
                $filter = [
                    'status' => 'work',
                ];
                if ($category) {
                    if (is_numeric($category)) {
                        $buf = ModelNewsCategory::fetch(['id' => $category]);
                        $category = $buf->path;
                    }

                    $filter['category'] = $category;
                }

                return CollectionNews::fetch($filter, [app()->storage->value('module_news', 'orderBy', 'id') => 'desc'], $limit, $offset)->calcRowsCount();
            }),
            new Twig_SimpleFunction('news_category_fetch', function ($limit = null) {
                return CollectionNewsCategory::fetch(['slider' => [null, '0'], 'status' => 'work'], ['position' => 'asc'], $limit);
            }),
        ];
    }

    // обработка статистики
    public static function getStatistic()
    {
        $category = CollectionNewsCategory::fetch(['status' => 'work']);

        return [
            [
                'title' => 'Записей',
                'value' => CollectionNews::fetch(['status' => 'work'])->count(),
            ],
            [
                'title' => 'Категорий записей',
                'value' => $category->where('slider', '!=', 1)->count(),
            ],
            [
                'title' => 'Категорий слайдеров',
                'value' => $category->where('slider', '==', 1)->count(),
            ],
        ];
    }
}
