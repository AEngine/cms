<?php

namespace News\Model;

use AEngine\Orchid\Filter as Filter;
use AEngine\Orchid\Support\Str;
use DateTime;
use Doctrine\ORM\Mapping\Cache;
use Doctrine\ORM\Mapping\Column;
use Doctrine\ORM\Mapping\Entity;
use Doctrine\ORM\Mapping\GeneratedValue;
use Doctrine\ORM\Mapping\Id;
use Doctrine\ORM\Mapping\Table;
use ORMFilterModel;
use Reference\Date as ReferenceDate;
use Upload\Collection\File as CollectionFile;

/**
 * @Entity()
 * @Table(name="News")
 * @Cache()
 */
class News extends ORMFilterModel
{
    /**
     * @Id
     * @Column(type="integer")
     * @GeneratedValue
     * @Filter\Lead\Integer()
     *
     * @var int
     */
    public $id;

    /**
     * @Column(type="string")
     * @Filter\Required()
     * @Filter\Lead\Str()
     * @Filter\Check\ValueNotEmpty(message="Поле не может быть пустым")
     *
     * @var string
     */
    public $title;

    /**
     * @Column(type="text")
     * @Filter\Lead\Str()
     *
     * @var string
     */
    public $description;

    /**
     * @Column(type="text")
     * @Filter\Required()
     * @Filter\Lead\Str()
     * @Filter\Check\ValueNotEmpty(message="Поле не может быть пустым")
     *
     * @var string
     */
    public $content;

    /**
     * @Column(type="string")
     * @Filter\Lead\Str()
     * @Filter\Lead\Lowercase()
     *
     * @var string
     */
    public $path;

    /**
     * @Column(type="string")
     * @Filter\Lead\Str()
     * @Filter\Lead\Lowercase()
     *
     * @var string
     */
    public $category;

    /**
     * @Column(type="string")
     * @Filter\Lead\Str()
     * @Filter\Check\InValues({'work', 'archive', 'delete'})
     *
     * @var string
     */
    public $status = 'work';

    /**
     * @Column(type="datetime")
     *
     * @var DateTime
     */
    public $date;

    /**
     * @var CollectionFile
     */
    public $files;

    public function replace(array $data)
    {
        parent::replace($data);

        if (!$this->description) {
            $this->description = $this->content;
        }

        $this->path = str_replace('--', '', preg_replace('/[^a-z]/ui', '-', strtolower(Str::translate(trim($this->title)))));

        if (is_string($this->date) || !$this->date) {
            $time = 'now';

            if ($this->date) {
                $time = date(ReferenceDate::DATE, strtotime($this->date));
            }

            $this->date = new DateTime($time);
        }

        return $this;
    }

    /**
     * Возвращает путь новости
     *
     * @return string
     */
    public function path()
    {
        return '/' . $this->category . '/' . $this->path;
    }

    /**
     * Возвращает файлы
     *
     * @return \ORMCollection
     */
    public function getFiles()
    {
        if (!blank($this->files)) {
            return $this->files;
        }

        $this->files = CollectionFile::fetch(['item' => 'news', 'item_id' => $this->id]);

        return $this->files;
    }
}
