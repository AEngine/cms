<?php

namespace News\Model;

use AEngine\Orchid\Filter as Filter;
use AEngine\Orchid\Support\Str;
use Doctrine\ORM\Mapping\Cache;
use Doctrine\ORM\Mapping\Column;
use Doctrine\ORM\Mapping\Entity;
use Doctrine\ORM\Mapping\GeneratedValue;
use Doctrine\ORM\Mapping\Id;
use Doctrine\ORM\Mapping\Table;
use ORMFilterModel;

/**
 * @Entity()
 * @Table(name="News_Category")
 * @Cache()
 */
class NewsCategory extends ORMFilterModel
{
    /**
     * @Id
     * @Column(type="integer")
     * @GeneratedValue
     * @Filter\Lead\Integer()
     *
     * @var int
     */
    public $id;

    /**
     * @Column(type="string")
     * @Filter\Required()
     * @Filter\Lead\Str()
     * @Filter\Check\ValueNotEmpty(message="Поле не может быть пустым")
     *
     * @var string
     */
    public $title;

    /**
     * @Column(type="text")
     * @Filter\Lead\Str()
     *
     * @var string
     */
    public $description;

    /**
     * @Column(type="integer", nullable=true)
     * @Filter\Lead\Integer()
     *
     * @var int
     */
    public $slider;

    /**
     * @Column(type="string")
     * @Filter\Lead\Str()
     * @Filter\Lead\Lowercase()
     *
     * @var string
     */
    public $path;

    /**
     * @Column(type="integer")
     * @Filter\Lead\Integer()
     *
     * @var int
     */
    public $position = 0;

    /**
     * @Column(type="string")
     * @Filter\Lead\Str()
     * @Filter\Check\InValues({'work', 'delete'})
     *
     * @var string
     */
    public $status = 'work';

    public function replace(array $data)
    {
        parent::replace($data);

        if (!$this->path) {
            $this->path = '/' . str_replace('--', '', preg_replace('/[^a-z]/ui', '-', strtolower(Str::translate(trim($this->title)))));
        }

        return $this;
    }
}
