<?php

namespace Panel {

    use AEngine\Orchid\App;
    use AEngine\Orchid\Collection;
    use AEngine\Orchid\Http\Request;
    use AEngine\Orchid\Http\Response;
    use AEngine\Orchid\Support\Str;
    use CMSModule;
    use PragmaRX\Google2FA\Google2FA;
    use Upload\Collection\File;

    class ModulePanel extends CMSModule
    {
        public static $priority = 10000;

        /**
         * @var boolean
         */
        private static $status = false;

        /**
         * @param App $app
         */
        public static function onInit(App $app)
        {
            // проверка авторизации
            $request = $app->request();
            $userHash = $request->getCookieParam('admin-panel-security');

            if ($userHash) {
                $data = [
                    'agent' => $request->getServerParam('HTTP_USER_AGENT'),
                    'ip' => $request->getServerParam('REMOTE_ADDR'),
                ];
                $hash = hash('sha256', $data['agent'] . $data['ip']);

                if ($userHash === $hash) {
                    static::$status = true;
                }
            }

            // если пользователь не авторизирован, всегда показываем авторизацию
            if (ModulePanel::$status === false) {
                static::addRoute('any', '/cup*', function (Request $req, Response $res) {
                    if ($req->isPost()) {
                        $password = $this->storage->value('module_panel', 'password', 'root');

                        if ($req->getParam('password', '') === $password) {
                            $valid = true;
                            $data = [
                                'agent' => $req->getServerParam('HTTP_USER_AGENT'),
                                'ip' => $req->getServerParam('REMOTE_ADDR'),
                            ];

                            if ($req->getParam('recaptcha', false) !== false) {
                                $valid = \System::checkReCAPTCHA([
                                    'response' => $req->getParam('recaptcha'),
                                    'remoteip' => $data['ip'],
                                ]);
                            }

                            // проверка авторизации
                            if ($authenticator = $this->storage->value('module_panel', 'authenticator', false)) {
                                $google2fa = new Google2FA();
                                $valid = $google2fa->verifyKey($authenticator, $req->getParam('authenticator', ''));
                            }

                            // если все хорошо
                            if ($valid === true) {
                                $hash = hash('sha256', $data['agent'] . $data['ip']);

                                setcookie('admin-panel-security', $hash, time() + \Reference\Date::YEAR, '/');

                                $this->logger->info('Login successful');

                                return $res->withAddedHeader('Location', '/cup');
                            }
                        }

                        $this->logger->warn('Login attempt');
                    }

                    return render($req, $res, 'panel/login.html');
                }, PHP_INT_MAX);
            }

            if (panel_is_admin()) {
                // хранилище элементов меню
                $app->storage->set('panel_navbar', []);

                // админ панель
                static::addRouteGroup('/cup', function () {
                    // главная страница
                    static::addRoute('any', '', function (Request $req, Response $res) {
                        $bin = [];
                        $statistic = [];

                        foreach (CMSModule::getLoaded() as $module) {
                            $bin = array_merge($bin, call_user_func([$module['class'], 'getBin']));
                            $statistic = array_merge(call_user_func([$module['class'], 'getStatistic']), $statistic);
                        }

                        return render($req, $res, 'panel/dashboard.html', [
                            'bin' => $bin,
                            'statistic' => $statistic,
                        ]);
                    });

                    // список файлов
                    static::addRoute(['get', 'post'], '/files', function (Request $req, Response $res) {
                        return render($req, $res, 'panel/files.html', ['list' => File::fetch()]);
                    });

                    // удаление файла
                    static::addRoute(['post'], '/files/:id/delete', function (Request $req, Response $res, $args) {
                        if(($file = \Upload\Model\File::fetch(['id' => $args['id']])) !== null) {
                            $file->remove();
                        }

                        return $res->withAddedHeader('Location', '/cup/files');
                    });

                    // список настроеки и их редактирование
                    static::addRoute(['get', 'post'], '/settings', function (Request $req, Response $res) {
                        if ($req->isPost()) {
                            $this->storage->change($req->getParam('namespace'), $req->getParams());

                            return $res->withAddedHeader('Location', '/cup/settings');
                        }

                        /**
                         * @var Collection $list
                         */
                        $list = $this->storage->get()->filter(function ($item, $key) { return str_starts_with('module_', $key); });

                        return render($req, $res, 'panel/settings.html', ['list' => $list]);
                    });

                    // журнал
                    static::addRoute(['get', 'post'], '/log', function (Request $req, Response $res) {
                        $rows = [];
                        $path = $this->path->get('var:log/process.log');

                        if ($path !== false) {
                            $rows = array_slice(file($path), -100, 100, true);

                            foreach ($rows as &$row) {
                                $row = trim($row);

                                $buf = [
                                    'date' => '',
                                    'level' => '',
                                    'message' => '',
                                    'data' => [],
                                    'client' => [],
                                ];

                                // вытягиваем информацию
                                preg_match('/^\[(?<date>[\d\-\s\:]+)\]\s(?<level>DEBUG|INFO|NOTICE|WARNING|ERROR|CRITICAL|ALERT|EMERGENCY)\:\s(?<message>[\d\w\s\:\,\.]+)/', $row, $matches);
                                $buf['date'] = $matches['date'];
                                $buf['level'] = $matches['level'];
                                $buf['message'] = $matches['message'];

                                // вытягиваем нагрузку
                                preg_match_all('/\{(?:[^{}]|(?R))*\}/', $row, $matches);
                                $buf['data'] = count($matches[0]) == 2 ? json_decode($matches[0][0], true) : [];
                                $buf = array_merge($buf, json_decode(array_last($matches[0]), true));

                                $row = $buf;
                            }
                        }

                        return render($req, $res, 'panel/log.html', ['list' => array_reverse($rows, true)]);
                    });

                    // bin
                    static::addRoute('any', '/bin', function (Request $req, Response $res) {
                        if ($req->isPost()) {
                            $script = $req->getParam('script', null);

                            if ($script && class_exists($script)) {
                                ini_set('max_execution_time', 900);

                                call_user_func([$script, 'exec']);
                            }
                        }

                        return $res->withAddedHeader('location', '/cup');
                    });

                    // выход
                    static::addRoute('get', '/logout', function (Request $req, Response $res) {
                        setcookie('admin-panel-security', '', time() - \Reference\Date::DAY, '/');

                        return $res->withAddedHeader('location', '/cup');
                    });
                });

                // пункт меню
                panel_navbar_add([
                    'title' => 'Дашборд',
                    'icon' => 'fa-home',
                    'link' => '/',
                    'items' => [
                        [
                            'title' => 'Обзор',
                        ],
                        [
                            'title' => 'Файлы',
                            'link' => '/files',
                        ],
                        [
                            'title' => 'Настройки',
                            'link' => '/settings',
                        ],
                        [
                            'title' => 'Журнал',
                            'link' => '/log',
                        ]
                    ],
                    'position' => PHP_INT_MIN,
                ]);
            }
        }

        /**
         * @return bool
         */
        public static function isAdmin()
        {
            return static::$status;
        }

        // настройки модуля
        public static function getSettings()
        {
            $google2fa = new Google2FA();
            $secretKey = $google2fa->generateSecretKey();
            $inlineImg = $google2fa->getQRCodeInline(
                'AEngine OmniCMS',
                $_SERVER['HTTP_HOST'],
                $secretKey,
                100
            );

            return [
                'title' => 'Панель управления',
                'description' => 'Настройки панели управления',
                'params' => [
                    'password' => [
                        'title' => 'Пароль',
                        'description' => 'Примечание: в целях безопасности, никому не сообщайте свой пароль',
                        'value' => 'root',
                    ],
                    'authenticator' => [
                        'title' => 'Google Authenticator',
                        'description' => 'Дополнительные одноразовые пароли для доступа к панели, включаются после ввода секретного кода',
                        'html' => '<button type="button" class="btn btn-sm btn-default waves-effect" data-toggle="popover" data-placement="bottom" data-html="true" data-content="1. Скопируйте секретный код;<br/>2. Отсканируйте QR код;<br/>3. Убедитесь в правильности;<br/>4. Сохраните настройки;<br/><div class=\'text-center\'><img src=\'' . $inlineImg . '\' width=\'100\'><br/>'. $secretKey .'</div>"><i class="fa fa-question-circle"></i></button>',
                        'placeholder' => 'Секретный код',
                    ],
                ],
            ];
        }

        public static function getStatistic()
        {
            /**
             * @var \Doctrine\ORM\EntityManager $em
             */
            $em = app()->entityManager;

            $result = [];

            $sizeStmt = $em->getConnection()->prepare('pragma page_size;');
            $sizeStmt->execute();
            $result[] = ['title' => 'Размер базы данных', 'value' => Str::convertSize($sizeStmt->fetchColumn())];

            $io = popen('/usr/bin/du -sk ' . app()->path->get('cache:'), 'r');
            $size = fgets($io, 4096);
            $size = substr($size, 0, strpos($size, "\t"));
            pclose($io);
            $result[] = ['title' => 'Размер кеш данных', 'value' => $size ? Str::convertSize((int)$size) : '-'];

            return $result;
        }
    }
}

namespace {

    use Panel\ModulePanel;
    use Support\Storage;

    function panel_is_admin()
    {
        return ModulePanel::isAdmin();
    }

    function panel_navbar_add(array $item)
    {
        if (!ModulePanel::isAdmin()) return;

        $default = [
            'title' => '',
            'icon' => '',
            'link' => '',
            'items' => [],
            'position' => 0,
        ];
        $item = array_merge($default, $item);
        $item['link'] = '/cup' . rtrim($item['link'], '/');

        // внутренние ссылки
        foreach ($item['items'] as &$i) {
            $i = array_merge($default, $i);
            $i['link'] = $item['link'] . rtrim($i['link'], '/');
        }

        /**
         * @var Storage $storage
         */
        $storage = app()->storage;

        $buf = $storage->get('panel_navbar');
        $buf[] = $item;

        usort($buf, function ($a, $b) {
            return $a['position'] > $b['position'];
        });

        $storage->set('panel_navbar', $buf);
    }
}
