<?php

namespace Upload\Model;

use AEngine\Orchid\Filter as Filter;
use DateTime;
use Doctrine\ORM\Mapping\Cache;
use Doctrine\ORM\Mapping\Column;
use Doctrine\ORM\Mapping\Entity;
use Doctrine\ORM\Mapping\GeneratedValue;
use Doctrine\ORM\Mapping\Id;
use Doctrine\ORM\Mapping\Table;
use Doctrine\ORM\Mapping\UniqueConstraint;
use ORMFilterModel;
use Upload\Collection\File as CollectionFile;

/**
 * @Entity()
 * @Table(name="Files",
 *     uniqueConstraints={
 *          @UniqueConstraint(name="file_hash", columns={"hash"})
 *     }
 * )
 * @Cache()
 */
class File extends ORMFilterModel
{
    const CONVERT = '/usr/bin/convert';

    public static $variants = [
        'small' => [
            'size' => 576,
            'command' => '-resize x576\> -set comment \'AEngine Official\'',
        ],
        'medium' => [
            'size' => 768,
            'command' => '-resize x768\> -set comment \'AEngine Official\'',
        ],
        'large' => [
            'size' => 992,
            'command' => '-resize x992\> -set comment \'AEngine Official\'',
        ],
    ];

    /**
     * @Id
     * @Column(type="integer")
     * @GeneratedValue
     * @Filter\Lead\Integer()
     *
     * @var int
     */
    public $id;

    /**
     * @Column(type="string")
     * @Filter\Required()
     * @Filter\Lead\Str()
     * @Filter\Check\ValueNotEmpty(message="Поле не может быть пустым")
     *
     * @var string
     */
    public $name;

    /**
     * @Column(type="string")
     * @Filter\Required()
     * @Filter\Lead\Str()
     * @Filter\Check\ValueNotEmpty(message="Поле не может быть пустым")
     *
     * @var string
     */
    public $type;

    /**
     * @Column(type="integer")
     * @Filter\Required()
     * @Filter\Lead\Integer()
     * @Filter\Check\ValueNotEmpty(message="Поле не может быть пустым")
     *
     * @var int
     */
    public $size;

    /**
     * @Column(type="string")
     * @Filter\Required()
     * @Filter\Lead\Str()
     *
     * @var string
     */
    public $salt;

    /**
     * @Column(type="string")
     * @Filter\Required()
     * @Filter\Lead\Str()
     *
     * @var string
     */
    public $hash;

    /**
     * @Column(type="string", nullable=true)
     * @Filter\Lead\Str()
     *
     * @var string
     */
    public $item;

    /**
     * @Column(type="integer", nullable=true)
     * @Filter\Lead\Integer()
     *
     * @var string
     */
    public $item_id;

    /**
     * @Column(type="datetime")
     *
     * @var DateTime
     */
    public $date;

    public function replace(array $data)
    {
        parent::replace($data);

        if (!$this->hash) {
            $this->hash = '';
        }

        if (!$this->date) {
            $this->date = new DateTime();
        }

        return $this;
    }

    public function convert()
    {
        $converted = [];

        if (file_exists(self::CONVERT)) {
            $uploads = app()->path->get('uploads:');
            $folder = $uploads . $this->salt;
            $original = $folder . '/' . $this->name;

            $size = getimagesize($original);

            if (str_starts_with('image/', $size['mime'])) {
                foreach (static::$variants as $variant => $args) {
                    $path = $folder . '/' . $variant;
                    $child = $path . '/' . $this->name;

                    if (!file_exists($child)) {
                        if (!is_dir($path)) {
                            mkdir($path);
                        }

                        if (($size[0] >= $args['size'] || $size[1] >= $args['size'])) {
                            exec(self::CONVERT . " '" . $original . "' " . $args['command'] . " " . $child);
                        } else {
                            copy($original, $child);
                        }
                    }
                }
            }
        }

        return $converted;
    }

    /**
     * Возвращает путь файла
     *
     * @param string $folder
     *
     * @return string
     */
    public function path($folder = '')
    {
        return app()->path->toUrl('uploads:' . $this->salt . ($folder ? '/' . $folder . '/' : '/') . $this->name);
    }

    /**
     * Возвращает размер файла
     *
     * @return string
     */
    public function size()
    {
        return str_convert_size($this->size);
    }

    public function remove()
    {
        $app = app();
        $app->logger->info('Remove file ' . $this->name);
        $uploads = $app->path->get('uploads:');

        $folder = $uploads . $this->salt;
        $path = $folder . '/' . $this->name;

        if (file_exists($path)) {
            foreach (array_keys(\Upload\Model\File::$variants) as $variant) {
                if (file_exists($folder . '/' . $variant)) {
                    unlink($folder . '/' . $variant . '/' . $this->name);
                    rmdir($folder . '/' . $variant);
                }
            }

            unlink($path);
            rmdir($folder);
        }

        parent::remove();
    }
}
