<?php

namespace Upload {

    use AEngine\Orchid\App;
    use AEngine\Orchid\Collection;
    use AEngine\Orchid\Http\Request;
    use AEngine\Orchid\Http\Response;
    use AEngine\Orchid\Http\UploadedFile;
    use AEngine\Orchid\Support\Str;
    use CMSModule;
    use Doctrine\DBAL\Exception\UniqueConstraintViolationException;
    use Psr\Http\Message\UploadedFileInterface;
    use RuntimeException;
    use Upload\Collection\File as CollectionFile;
    use Upload\Model\File as ModelFile;

    class ModuleUpload extends CMSModule
    {
        public static $priority = 1000;

        public static function onInit(App $app)
        {
            $router = $app->router();

            $router->post('/file/upload', function (Request $req, Response $res) {
                $uploaded = ModuleUpload::fromUploadedFiles($req->getUploadedFiles());

                return $res
                    ->withAddedHeader('Content-Type', 'application/json')
                    ->write($uploaded->toJson());
            });
        }

        /**
         * Обрабатывает загруженные методом POST файлы
         *
         * @param UploadedFileInterface[] $uploadedFiles
         * @param bool                    $save
         *
         * @return Collection
         * @throws \Doctrine\ORM\ORMException
         * @throws \Doctrine\ORM\OptimisticLockException
         */
        public static function fromUploadedFiles(array $uploadedFiles, $save = true)
        {
            $app = app();
            $uploadPath = $app->path->get('uploads:');
            $uploaded = [];

            foreach ($uploadedFiles as $field => $files) {
                if (!is_array($files)) {
                    $files = [$files];
                }

                /**
                 * @var UploadedFile $file
                 */
                foreach ($files as $file) {
                    $salt = uniqid();
                    $name = Str::translate(strtolower($file->getClientFilename()));
                    $path = $uploadPath . $salt . '/' . $name;

                    if (!file_exists($uploadPath . $salt)) {
                        mkdir($uploadPath . $salt);
                    }

                    $file->moveTo($path);
                    $hash = md5_file($path);

                    $file = new ModelFile([
                        'name' => $name,
                        'type' => $file->getClientMediaType(),
                        'size' => $file->getSize(),
                        'salt' => $salt,
                        'hash' => $hash,
                    ]);

                    if ($file->filter() !== true) {
                        throw new RuntimeException('Ошибка при проверке файла');
                    }

                    if ($save) {
                        try {
                            $uploaded[$field][] = $file->save();
                            $app->logger->info('File has been uploaded', ['id' => $file->id]);
                        } catch (UniqueConstraintViolationException $e) { // вероятно пытались загрузить файл дважды
                            // удаляем перемещенный файл
                            unlink($path);

                            // удаляем папку для файла
                            rmdir($uploadPath . $salt);

                            // достаем из базы файл
                            $file = ModelFile::fetch(['hash' => $hash]);
                            if (!$file->isEmpty()) {
                                $uploaded[$field][] = $file;
                            } else {
                                $app->logger->warning('Failed to save file information', ['name' => $file->name]);
                            }
                        }
                    }

                    break;
                }
            }

            return collect($uploaded);
        }

        /**
         * Загружает файл по ссылке
         *
         * @param string $remotePath
         * @param bool   $save
         *
         * @return null|ModelFile
         * @throws \Doctrine\ORM\ORMException
         * @throws \Doctrine\ORM\OptimisticLockException
         */
        public static function fromRemote($remotePath, $save = true)
        {
            $app = app();
            $uploadPath = $app->path->get('uploads:');
            $fileRaw = static::getFileIfExists($remotePath);

            if ($fileRaw) {
                $fileData = pathinfo($remotePath);

                $salt = uniqid();
                $name = Str::translate(strtolower($fileData['basename']));
                $path = $uploadPath . $salt . '/' . $name;

                if (!file_exists($uploadPath . $salt)) {
                    mkdir($uploadPath . $salt);
                }

                file_put_contents($path, $fileRaw);
                $hash = md5_file($path);

                $file = new ModelFile([
                    'name' => $name,
                    'type' => filetype($path), // todo
                    'size' => filesize($path),
                    'salt' => $salt,
                    'hash' => $hash,
                ]);

                if ($file->filter() !== true) {
                    throw new RuntimeException('Ошибка при проверке файла');
                }

                if ($save) {
                    try {
                        $file->save();
                        $app->logger->info('File has been uploaded from remote', ['id' => $file->id]);

                        return $file;
                    } catch (UniqueConstraintViolationException $e) { // вероятно пытались загрузить файл дважды
                        // удаляем перемещенный файл
                        unlink($path);

                        // удаляем папку для файла
                        rmdir($uploadPath . $salt);

                        // достаем из базы файл
                        $file = ModelFile::fetch(['hash' => $hash]);
                        if (!$file->isEmpty()) {
                            return $file;
                        } else {
                            $app->logger->warning('Failed to save file information', ['path' => $remotePath]);
                        }
                    }
                }

                return $file;
            }

            return null;
        }

        /**
         * Ищет файл по указанному пути
         *
         * @param $rawPath
         *
         * @return mixed
         */
        protected static function getFileIfExists($rawPath)
        {
            $entities = ['%21', '%2A', '%27', '%28', '%29', '%3B', '%3A', '%40', '%26', '%3D', '%2B', '%24', '%2C', '%2F', '%3F', '%25', '%23', '%5B', '%5D'];
            $replacements = ['!', '*', "'", "(", ")", ";", ":", "@", "&", "=", "+", "$", ",", "/", "?", "%", "#", "[", "]"];

            $path = str_replace($entities, $replacements, urlencode($rawPath));

            $headers = get_headers($path);
            $code = substr($headers[0], 9, 3);

            if ($code == 200) {
                return @file_get_contents($path);
            }

            app()->logger->error('File upload failed from remote', ['code' => $code, 'path' => $path]);

            return null;
        }

        public static function getStatistic()
        {
            return [
                [
                    'title' => 'Файлов загружено',
                    'value' => CollectionFile::fetch()->count(),
                ],
            ];
        }
    }
}

namespace {

    use Upload\ModuleUpload;

    /**
     * Загружает и возвращает файлы из UploadedFileInterface
     *
     * @param array $uploadedFiles
     * @param bool  $save
     *
     * @return \AEngine\Orchid\Collection
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    function upload_from_uploaded_files(array $uploadedFiles, $save = true)
    {
        return ModuleUpload::fromUploadedFiles($uploadedFiles, $save);
    }

    /**
     * Загружает и возвращает файл по указанной ссылке
     *
     * @param string $path
     * @param bool   $save
     *
     * @return null|\Upload\Model\File|\Upload\Model\Image
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    function upload_from_remote($path, $save = true)
    {
        return ModuleUpload::fromRemote($path, $save);
    }
}
