<?php

namespace Upload\Collection;

use Doctrine\ORM\Mapping\Cache;
use ORMCollection;

/**
 * @Cache()
 */
class File extends ORMCollection
{
    public static $model = \Upload\Model\File::class;
}
