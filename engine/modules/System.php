<?php

use AEngine\Orchid\App;
use AEngine\Orchid\Http\Request;
use AEngine\Orchid\Http\Response;
use AEngine\Orchid\Support\Session;
use PHPMailer\PHPMailer\PHPMailer;

class System extends CMSModule
{
    public static $priority = 10001;

    public static function onInit(App $app)
    {
        // начинаем сессию
        Session::create('visit');

        // перенаправление если адрес кончается на слеш
        static::addRoute('any', '#\w+\/$#', function (Request $req, Response $res) {
            return $res
                ->withStatus(308)
                ->withAddedHeader('Location', rtrim($req->getUri()->getPath(), '/'));
        }, PHP_INT_MAX);
    }

    /**
     * Метод проверки reCAPTCHA
     *
     * @param array $data
     *
     * @return bool
     */
    public static function checkReCAPTCHA(array $data = [])
    {
        $default = [
            'secret' => app()->storage->value('module_system', 'recaptcha_private'),
            'response' => '',
            'remoteip' => '',
        ];
        $data = array_merge($default, $data);

        if ($data['secret'] && app()->storage->value('module_system', 'recaptcha') == 'on') {
            $query = http_build_query($data);
            $verify = json_decode(file_get_contents('https://www.google.com/recaptcha/api/siteverify', false, stream_context_create([
                'http' => [
                    'method' => 'POST',
                    'header' => "Content-Type: application/x-www-form-urlencoded\r\n".
                        "Content-Length: ".strlen($query)."\r\n",
                    'content' => $query,
                ],
            ])));

            app()->logger->info('Check reCAPTCHA', ['verify' => $verify->success]);

            return $verify->success;
        }

        return true;
    }

    /**
     * Метод для отправки почты с сайта
     *
     * @param array $data
     *
     * @return PHPMailer
     * @throws \PHPMailer\PHPMailer\Exception
     */
    public static function sendMail(array $data = [])
    {
        $app = app();

        $default = [
            'subject' => $app->storage->value('module_system', 'smtp_subject'),
            'to' => '', // string|array(address=>name)
            'cc' => '', // string|array(address=>name)
            'bcc' => '', // string|array(address=>name)
            'body' => '',
            'isHtml' => false,
            'attachments' => [],
            'auto_send' => true,
        ];
        $data = array_merge($default, $data);

        $mail = new PHPMailer(false);
        $mail->isSMTP();
        $mail->set('SMTPSecure', $app->storage->value('module_system', 'smtp_secure'));
        $mail->set('Host', $app->storage->value('module_system', 'smtp_host'));
        $mail->set('Port', $app->storage->value('module_system', 'smtp_port'));
        $mail->set('CharSet', 'utf-8');
        $mail->set('SMTPAuth', true);
        $mail->set('Username', $app->storage->value('module_system', 'smtp_login'));
        $mail->set('Password', $app->storage->value('module_system', 'smtp_pass'));
        $mail->setFrom(
            $app->storage->value('module_system', 'smtp_from'),
            $app->storage->value('module_system', 'smtp_from_name')
        );

        // Кому
        if ($data['to']) {
            foreach ((array)$data['to'] as $address => $name) {
                if (is_numeric($address)) {
                    $address = $name;
                    $name = '';
                }

                $mail->addAddress($address, $name);
            }
        }

        // Копия
        if ($data['cc']) {
            foreach ((array)$data['cc'] as $address => $name) {
                if (is_numeric($address)) {
                    $address = $name;
                    $name = '';
                }

                $mail->addCC($address, $name);
            }
        }

        // Скрытая копия
        if ($data['bcc']) {
            foreach ((array)$data['bcc'] as $address => $name) {
                if (is_numeric($address)) {
                    $address = $name;
                    $name = '';
                }

                $mail->addBCC($address, $name);
            }
        }

        $mail->set('Subject', $data['subject']);
        $mail->set('Body', $data['body']);
        $mail->isHTML($data['isHtml']);

        foreach ((array)$data['attachments'] as $name => $file) {
            $mail->addAttachment($file, $name);
        }

        if ($data['auto_send']) { $mail->send(); }

        return $mail;
    }

    // настройки модуля
    public static function getSettings()
    {
        return [
            'title' => 'Система',
            'description' => 'Настройки в этой секции влияют на весь сайт',
            'params' => [
                'title' => [
                    'title' => 'Название сайта',
                    'description' => 'например: "Моя домашняя страница"',
                    'value' => 'AEngine OmniCMS',
                ],
                'homepage' => [
                    'title' => 'Домашняя страница сайта',
                    'description' => 'Укажите имя основного домена на котором располагается ваш сайт. Например: http://yoursite.com/',
                    'value' => '',
                ],
                'description' => [
                    'title' => 'Описание (Description) сайта',
                    'description' => 'Краткое описание, не более 200 символов',
                    'value' => 'Демонстрационная страница движка AEngine OmniCMS',
                ],
                'keywords' => [
                    'title' => 'Ключевые слова (Keywords) для сайта',
                    'description' => 'Введите через запятую основные ключевые слова для вашего сайта',
                    'value' => 'AEngine, OmniCMS, Engine, CMS, PHP движок',
                    'type' => 'textarea',
                ],
                'theme' => [
                    'title' => 'Название шаблона',
                    'description' => 'Введите название главного шаблона',
                    'value' => 'Default',
                ],

                // reCAPTCHA
                ['title' => 'reCAPTCHA', 'type' => static::PARAM_TYPE_SEPARATOR],
                'recaptcha' => [
                    'title' => 'Включение и выключение',
                    'description' => 'Защита от роботов',
                    'type' => static::PARAM_TYPE_SWITCH,
                    'value' => 'off',
                ],
                'recaptcha_public' => [
                    'title' => 'Ключ',
                    'description' => 'Этот ключ будет использован в HTML-коде сайта на устройства пользователей',
                    'placeholder' => '6Le_gY8UAAAAAKv9EUVJxlAQTchtx2ACG6B616Eu',
                ],
                'recaptcha_private' => [
                    'title' => 'Секретный ключ',
                    'description' => 'Этот ключ нужен для связи между сайтом и сервисом reCAPTCHA. Никому его не сообщайте',
                    'placeholder' => '6Le_gY8UAAAAAAx1_Lv3aKo-L1QnxE2QWhOHCgxS',
                ],

                // настройки почты
                ['title' => 'SMTP', 'type' => static::PARAM_TYPE_SEPARATOR],
                'smtp_from' => [
                    'title' => 'Системный E-Mail адрес',
                    'description' => 'От данного адреса будут отправляться сообщения сайта, например уведомления пользователей, рассылки, подтверждения и т.д. Примечание: некоторые бесплатные почтовые сервисы, например yandex.ru, требуют, чтобы в качестве E-Mail адреса отправителя был указан именно адрес, зарегистрированный на их почтовом сервисе',
                    'placeholder' => 'my.site@company.com',
                ],
                'smtp_from_name' => [
                    'title' => 'Имя отправителя',
                    'description' => 'Данное имя будет прикреплено к письму в качетве имени отправителя',
                    'value' => '',
                ],
                'smtp_subject' => [
                    'title' => 'Заголовок при отправке писем',
                    'description' => 'При отправке писем с сайта вы можете указать заголовок, который будет добавляться к почте отправителя. Например, вы можете там указать краткое название вашего сайта',
                    'value' => '',
                ],
                'smtp_host' => [
                    'title' => 'SMTP хост',
                    'description' => 'Обычно — localhost',
                    'placeholder' => 'smtp.yandex.ru',
                ],
                'smtp_port' => [
                    'title' => 'SMTP порт',
                    'description' => 'Обычно — 25',
                    'type' => static::PARAM_TYPE_SELECT,
                    'options' => [
                        25 => '25',
                        587 => '587',
                        465 => '465',
                    ],
                    'value' => 465,
                ],
                'smtp_secure' => [
                    'title' => 'Использовать защищенный протокол для отправки писем через SMTP сервер',
                    'description' => 'Выберите протокол шифрования при отправке писем с использованием SMTP сервера',
                    'type' => static::PARAM_TYPE_SELECT,
                    'options' => [
                        '' => 'Нет',
                        'ssl' => 'SSL',
                        'tls' => 'TLS',
                    ],
                    'value' => 'ssl',
                ],

                'smtp_login' => [
                    'title' => 'SMTP имя пользователя',
                    'description' => 'Не требуется в большинстве случаев, когда используется \'localhost\'',
                    'value' => '',
                ],
                'smtp_pass' => [
                    'title' => 'SMTP пароль',
                    'description' => 'Не требуется в большинстве случаев, когда используется \'localhost\'',
                    'value' => '',
                ],
            ],
        ];
    }

    public static function getBin()
    {
        $dedug = [
            'script' => \Bin\DebugMode::class,
        ];

        if (app()->isDebug()) {
            $dedug['title'] = 'Режим разработчика: ON';
            $dedug['button'] = 'Выключить';
            $dedug['button_variant'] = 'warning';
        } else {
            $dedug['title'] = 'Режим разработчика: OFF';
            $dedug['button'] = 'Включить';
        }

        return [
            $dedug,
            'cache' => [
                'title' => 'Обновить кеш',
                'script' => \Bin\BuildCache::class,
                'button_variant' => 'info',
            ],
            'sitemap' => [
                'title' => 'Обновить карту сайта',
                'script' => \Bin\UpdateSitemap::class,
                'button_variant' => 'info',
            ],
        ];
    }
}

/**
 * @param array $data
 *
 * @return bool
 */
function checkReCAPTCHA(array $data = [])
{
    return System::checkReCAPTCHA($data);
}

/**
 * @param array $data
 *
 * @return PHPMailer
 * @throws \PHPMailer\PHPMailer\Exception
 */
function sendMail(array $data = [])
{
    return System::sendMail($data);
}
