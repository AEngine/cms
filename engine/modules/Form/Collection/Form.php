<?php

namespace Form\Collection;

use Doctrine\ORM\Mapping\Cache;
use ORMCollection;

/**
 * @Cache()
 */
class Form extends ORMCollection
{
    public static $model = \Form\Model\Form::class;
}

