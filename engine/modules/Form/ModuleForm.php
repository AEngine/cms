<?php

namespace Form;

use AEngine\Orchid\App;
use AEngine\Orchid\Filter\Filter;
use AEngine\Orchid\Filter\TraitFilter;
use AEngine\Orchid\Http\Request;
use AEngine\Orchid\Http\Response;
use CMSModule;
use Form\Collection\Form as CollectionForm;
use Form\Model\Form as ModelForm;

class ModuleForm extends CMSModule
{
    /**
     * @param App $app
     */
    public static function onInit(App $app)
    {
        // принятие данных формы
        static::addRoute('any', '/form/:hash', function (Request $req, Response $res, $args) {
            $item = ModelForm::fetch($args + ['status' => 'work']);

            if ($item) {
                $res = $res->withStatus(400);

                if ($item->origin) {
                    // разрешаем удаленные запросы с методами
                    $res = $res->withAddedHeader('Access-Control-Allow-Methods', 'POST, OPTIONS');

                    // разруливаем разрешение на использование формы
                    if ($item->origin == '*') {
                        $res = $res->withAddedHeader('Access-Control-Allow-Origin', '*');
                    } else {
                        $valid = false;
                        $remote = $_SERVER['HTTP_ORIGIN'] ?? $_SERVER['HTTP_REFERER'];
                        $origins = explode(';', str_replace(' ', '', rtrim($item->origin, ';')));

                        foreach ($origins as $origin) {
                            $check = strpos($remote, $origin);

                            if ($check !== false && $check >= 0) {
                                $res = $res->withAddedHeader('Access-Control-Allow-Origin', $remote);
                                $valid = true;
                                break;
                            }
                        }

                        // если не нашли адрес в разрешениях - закрываемся
                        if ($valid === false) {
                            return $res->withStatus(403);
                        }
                    }
                }

                if ($req->isPost()) {
                    $valid = true;

                    $data = $req->getParams();

                    if ($req->getParam('recaptcha', false) !== false) {
                        $valid = \System::checkReCAPTCHA([
                            'response' => $req->getParam('recaptcha'),
                            'remoteip' => $req->getServerParam('REMOTE_ADDR'),
                        ]);
                    }

                    // если все хорошо
                    if ($valid === true) {
                        // подменяем адрес назначения, если разрешено в настройках
                        if (!$item->mailto_strong && $req->getParam('mailto', '')) {
                            $item->mailto = $req->getParam('mailto', '');
                        }

                        // делим адреса
                        $mailto = [];
                        foreach (array_map('trim', explode(';', $item->mailto)) as $key => $value) {
                            $buf = array_map('trim', explode(':', $value));

                            if (count($buf) == 2) {
                                $mailto[$buf[0]] = $buf[1];
                            } else {
                                $mailto[] = $buf[0];
                            }
                        }
                        $item->mailto = $mailto;

                        $body = '';
                        $isHtml = true;

                        // если есть текст сообщения
                        if ($item->message && $item->message != '<p><br></p>') {
                            // проверяем входящие данные
                            $filter = new class($data) extends Filter
                            {
                                use TraitFilter;
                            };

                            $filter->addGlobalRule($filter->leadEscape());
                            $filter->addGlobalRule($filter->leadTrim());

                            $check = $filter->run();

                            // если все ок - готовим сообщение
                            if ($check === true) {
                                $body = $this->view->fetchFromString($item->message, $data);
                            } else {
                                $res = $res->withStatus(406);
                            }
                        } else {
                            // может быть тело пришло в параметрах?
                            if ($buf = $req->getParam('body', false)) {
                                $body = $buf;
                            } else {
                                // если нет - делаем json
                                $body = json_encode(str_escape($data), JSON_UNESCAPED_UNICODE);
                                $isHtml = false;
                            }
                        }

                        // проверяем приложенные файлы
                        $attachments = [];

                        if (!empty($_FILES)) {
                            foreach ($_FILES as $file) {
                                if (is_array($file['name'])) {
                                    $attachments = array_merge($attachments, array_combine($file['name'], $file['tmp_name']) );
                                } else {
                                    $attachments[$file['name']] = $file['tmp_name'];
                                }
                            }
                        }

                        // отправляем адресату
                        $mail = sendMail([
                            'subject' => $item->title,
                            'to' => $item->mailto,
                            'body' => $body,
                            'isHtml' => $isHtml,
                            'attachments' => $attachments,
                        ]);

                        if (!$mail->isError()) {
                            $this->logger->info('FormSend: ' . $item->title, ['mailto' => $item->mailto]);
                            $res = $res->withStatus(200)->write('Message sent');
                        } else {
                            $this->logger->warn('FormSend: failed, not sent', ['mailto' => $item->mailto, 'error' => $mail->ErrorInfo]);
                            $res = $res->withStatus(520, 'Message not sent')->write('Message not sent');
                        }
                    } else {
                        $this->logger->warn('FormSend: failed attempt sent message');

                        $res = $res->withStatus(403)->write('Wrong recaptcha value');
                    }
                }
            }

            return $res;
        });


        if (panel_is_admin() === true) {
            // админ панель
            static::addRouteGroup('/cup/form', function () {
                // список форм
                static::addRoute('get', '', function (Request $req, Response $res) {
                    return render($req, $res, 'form/index.html', [
                        'list' => CollectionForm::fetch(['status' => 'work'], ['id' => 'desc']),
                    ]);
                });

                // добавление формы
                static::addRoute(['get', 'post'], '/add', function (Request $req, Response $res) {
                    if ($req->isPost()) {
                        $item = new ModelForm([
                            'title' => $req->getParam('title', ''),
                            'message' => $req->getParam('message', ''),
                            'origin' => $req->getParam('origin', ''),
                            'mailto' => $req->getParam('mailto', ''),
                            'mailto_strong' => $req->getParam('mailto_strong', ''),
                        ]);

                        $check = $item->filter();

                        if ($check === true) {
                            $item->save();
                            $this->logger->info('Form has been added', ['id' => $item->id]);

                            return $res->withAddedHeader('Location', '/cup/form');
                        }
                        $this->view->getEnvironment()->addGlobal('_error', $check);
                    }

                    return render($req, $res, 'form/form.html', ['_post' => $_POST]);
                });

                // редактирование формы
                static::addRoute(['get', 'post'], '/:id/edit', function (Request $req, Response $res, $args) {
                    $item = ModelForm::fetch($args + ['status' => 'work']);

                    if ($item) {
                        if ($req->isPost()) {
                            $item->replace([
                                'title' => $req->getParam('title', ''),
                                'message' => $req->getParam('message', ''),
                                'origin' => $req->getParam('origin', ''),
                                'mailto' => $req->getParam('mailto', ''),
                                'mailto_strong' => $req->getParam('mailto_strong', ''),
                            ]);

                            if ($req->getParam('hash', '')) {
                                $item->replace(['hash' => '']);
                            }

                            $check = $item->filter();

                            if ($check === true) {
                                $item->save();
                                $this->logger->info('Form has been edited', ['id' => $item->id]);

                                return $res->withAddedHeader('Location', '/cup/form');
                            }

                            $this->view->getEnvironment()->addGlobal('_error', $check);
                        } else {
                            // для отрисовки шаблона
                            $_POST = $item->toArray();
                        }

                        return render($req, $res, 'form/form.html', ['_post' => $_POST]);
                    }

                    return $res->withAddedHeader('Location', '/cup/form');
                });

                // удаление формы
                static::addRoute(['post'], '/:id/delete', function (Request $req, Response $res, $args) {
                    $item = ModelForm::fetch($args + ['status' => 'work']);

                    if ($item) {
                        $item->set('status', 'delete')->save();
                        $this->logger->info('Form has been mark as deleted', ['id' => $item->id]);
                    }

                    return $res->withAddedHeader('Location', '/cup/form');
                });
            });

            // пункт в меню
            panel_navbar_add([
                'title' => 'Формы',
                'icon' => 'fa-user',
                'link' => '/form',
                'items' => [
                    ['title' => 'Список'],
                ],
                'position' => 2,
            ]);
        }
    }

    // обработка статистики
    public static function getStatistic()
    {
        return [
            [
                'title' => 'Форм',
                'value' => CollectionForm::fetch(['status' => 'work'])->count(),
            ],
        ];
    }
}
