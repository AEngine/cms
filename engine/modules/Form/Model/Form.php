<?php

namespace Form\Model;

use AEngine\Orchid\Filter as Filter;
use AEngine\Orchid\Support\Str;
use DateTime;
use Doctrine\ORM\Mapping\Cache;
use Doctrine\ORM\Mapping\Column;
use Doctrine\ORM\Mapping\Entity;
use Doctrine\ORM\Mapping\GeneratedValue;
use Doctrine\ORM\Mapping\Id;
use Doctrine\ORM\Mapping\Table;
use ORMFilterModel;
use Reference\Date as ReferenceDate;

/**
 * @Entity()
 * @Table(name="Form")
 * @Cache()
 */
class Form extends ORMFilterModel
{
    /**
     * @Id
     * @Column(type="integer")
     * @GeneratedValue
     * @Filter\Lead\Integer()
     *
     * @var int
     */
    public $id;

    /**
     * @Column(type="string")
     * @Filter\Required()
     * @Filter\Lead\Str()
     * @Filter\Check\ValueNotEmpty(message="Поле не может быть пустым")
     *
     * @var string
     */
    public $title;

    /**
     * @Column(type="text")
     * @Filter\Lead\Str()
     *
     * @var string
     */
    public $message = "";

    /**
     * @Column(type="string")
     * @Filter\Required()
     * @Filter\Lead\Str()
     * @Filter\Lead\Lowercase()
     * @Filter\Check\ValueNotEmpty(message="Поле не может быть пустым")
     *
     * @var string
     */
    public $origin;

    /**
     * @Column(type="string")
     * @Filter\Lead\Str()
     * @Filter\Lead\Lowercase()
     * @Filter\Check\ValueNotEmpty(message="Поле не может быть пустым")
     *
     * @var string
     */
    public $hash;

    /**
     * @Column(type="string")
     * @Filter\Lead\Str()
     *
     * @var string
     */
    public $mailto;

    /**
     * @Column(type="boolean")
     * @Filter\Lead\Boolean(true, false)
     *
     * @var string
     */
    public $mailto_strong = false;

    /**
     * @Column(type="string")
     * @Filter\Lead\Str()
     * @Filter\Check\InValues({'work', 'delete'})
     *
     * @var string
     */
    public $status = 'work';

    /**
     * @Column(type="datetime")
     *
     * @var DateTime
     */
    public $date;

    public function replace(array $data)
    {
        parent::replace($data);

        if (!$this->hash) {
            $this->hash = md5(time());
        }

        if (is_string($this->date) || !$this->date) {
            $time = 'now';

            if ($this->date) {
                $time = date(ReferenceDate::DATE, strtotime($this->date));
            }

            $this->date = new DateTime($time);
        }

        return $this;
    }
}
