<?php

require_once(__DIR__ . '/init.php');

# settings
// theme default
(
    new \Support\StorageModel([
        'namespace' => 'module_system',
        'key' => 'theme',
        'value' => 'Default',
    ])
)->save();

// news sort
(
    new \Support\StorageModel([
        'namespace' => 'module_news',
        'key' => 'orderBy',
        'value' => 'date',
    ])
)->save();

# main page
(
    new \Page\Model\Page([
        'title' => 'Главная страница',
        'description' => 'Наша главная страница',
        'path' => '/',
        'template' => 'base.html',
        'content' => '',
    ])
)->save();

# about page
(
    new \Page\Model\Page([
        'title' => 'О нас',
        'description' => 'Страница рассказывает про нас',
        'path' => '/about',
        'template' => 'base.html',
        'content' => '
            <section class="bg-title-page p-t-40 p-b-50 flex-col-c-m" style="background-image: url(/themes/Default/images/heading-pages-06.jpg);">
                <h2 class="l-text2 t-center">
                    О нас
                </h2>
            </section>
            
            <section class="bgwhite p-t-66 p-b-38">
                <div class="container">
                    <div class="row">
                        <div class="col-md-4 p-b-30">
                            <div class="hov-img-zoom">
                                <img src="/themes/Default/images/banner-14.jpg" alt="IMG-ABOUT">
                            </div>
                        </div>
                        
                        <div class="col-md-8 p-b-30">
                            <h3 class="m-text26 p-t-15 p-b-16">
                                Наша история
                            </h3>
                            
                            <p class="p-b-28">
                                Значимость этих проблем настолько очевидна, что реализация намеченных плановых заданий представляет собой интересный эксперимент проверки новых предложений. Таким образом реализация намеченных плановых заданий требуют от нас анализа направлений прогрессивного развития. Повседневная практика показывает, что дальнейшее развитие различных форм деятельности позволяет оценить значение направлений прогрессивного развития. Задача организации, в особенности же новая модель организационной деятельности требуют от нас анализа форм развития. Не следует, однако забывать, что новая модель организационной деятельности позволяет оценить значение дальнейших направлений развития. Задача организации, в особенности же консультация с широким активом играет важную роль в формировании систем массового участия.
                            </p>
                            
                            <p class="p-b-28">
                                Равным образом дальнейшее развитие различных форм деятельности играет важную роль в формировании новых предложений. Задача организации, в особенности же сложившаяся структура организации требуют от нас анализа системы обучения кадров, соответствует насущным потребностям. Таким образом укрепление и развитие структуры обеспечивает широкому кругу (специалистов) участие в формировании соответствующий условий активизации.
                            </p>
                            
                            <p class="p-b-28">
                                Значимость этих проблем настолько очевидна, что укрепление и развитие структуры требуют от нас анализа систем массового участия. Таким образом реализация намеченных плановых заданий требуют от нас анализа новых предложений.
                            </p>
                            
                            <div class="bo13 p-l-29 m-l-9 p-b-10">
                                <p class="p-b-11">
                                    Не следует, однако забывать, что постоянный количественный рост и сфера нашей активности играет важную роль в формировании направлений прогрессивного развития. Не следует, однако забывать, что сложившаяся структура организации требуют от нас анализа модели развития.
                                </p>
                                
                                <span class="s-text7">
                                    - Шедевр
                                </span>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        ',
    ])
)->save();

# contact page
(
    new \Page\Model\Page([
        'title' => 'Контакты',
        'description' => 'Контакты с нами',
        'path' => '/contact',
        'template' => 'base.html',
        'content' => '
            <section class="bg-title-page p-t-40 p-b-50 flex-col-c-m" style="background-image: url(/themes/Default/images/heading-pages-06.jpg);">
                <h2 class="l-text2 t-center">
                    Контакты
                </h2>
            </section>
            
            <section class="bgwhite p-t-66 p-b-60">
                <div class="container">
                    <div class="row">
                        <div class="col-md-6 p-b-30">
                            <div class="p-r-20 p-r-0-lg">
                                <div class="contact-map size21" id="google_map" data-map-x="40.614439" data-map-y="-73.926781" data-pin="/themes/Default/images/icons/icon-position-map.png" data-scrollwhell="0" data-draggable="1"></div>
                            </div>
                        </div>
                        
                        <div class="col-md-6 p-b-30">
                            <form class="leave-comment">
                                <h4 class="m-text26 p-b-36 p-t-15">
                                    Отправьте ваше сообщение нам
                                </h4>
                                
                                <div class="bo4 of-hidden size15 m-b-20">
                                    <input class="sizefull s-text7 p-l-22 p-r-22" type="text" name="name" placeholder="Полное имя">
                                </div>
                                
                                <div class="bo4 of-hidden size15 m-b-20">
                                    <input class="sizefull s-text7 p-l-22 p-r-22" type="text" name="phone-number" placeholder="Телефон">
                                </div>
                                
                                <div class="bo4 of-hidden size15 m-b-20">
                                    <input class="sizefull s-text7 p-l-22 p-r-22" type="text" name="email" placeholder="E-Mail адрес">
                                </div>
                                
                                <textarea class="dis-block s-text7 size20 bo4 p-l-22 p-r-22 p-t-13 m-b-20" name="message" placeholder="Сообщение"></textarea>
                                
                                <div class="w-size25">
                                    <!-- Button -->
                                    <button class="flex-c-m size2 bg1 bo-rad-23 hov1 m-text3 trans-0-4">
                                        Отправить
                                    </button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </section>

            <script src="https://maps.googleapis.com/maps/api/js"></script>
            <script>
                document.addEventListener("DOMContentLoaded", function(){
					var map = new google.maps.Map(document.getElementById("google_map"), {
						zoom: 15,
						navigationControl: true,
						mapTypeControl: false,
						scaleControl: false,
						center: new google.maps.LatLng(49.844016, 24.026295),
						mapTypeId: google.maps.MapTypeId.ROADMAP
					});
                });
            </script>
        ',
    ])
)->save();

# news category
$nc = [
    [
        'title' => 'О скрипте',
        'description' => 'Описание скрипта и его возможностей',
        'path' => 'news',
    ],
    [
        'title' => 'В мире',
        'description' => 'События в мире: открытия, события, достижения',
    ],
    [
        'title' => 'Экономика',
        'description' => 'Упадки и росты',
    ],
    [
        'title' => 'Религия',
        'description' => 'В дружбе с богом, дальше от мира',
    ],
];
foreach ($nc as $item) {
    $category = new \News\Model\NewsCategory($item);
    $category->save();
}

# news rows
$nt = [
    [
        'title' => 'Добро пожаловать',
        'content' => 'Добро пожаловать на демонстрационную страницу движка AEngine OmniCMS. 
                      AEngine OmniCMS - это многопользовательский новостной движок, обладающий большими функциональными возможностями.
                      Движок предназначен, в первую очередь, для создания новостных блогов, сайтов с большим количеством информации.
                      В движок может быть интегрирован абсолютно любой дизайн, нет никаких ограничений по созданию шаблонов для него.
                      Еще одной ключевой особенностью AEngine OmniCMS является низкая нагрузка на системные требования.
                      Так же движок оптимизирован под поисковые системы.',
        'path' => 'welcome',
        'category' => 'news',
        'date' => '03.12.2018',
    ],
    [
        'title' => 'Бесплатно, абсолютно',
        'content' => 'В данный момент движок AEngine OmniCMS распространяется под лицензией MIT, что позволяет вам: 
                      вам свободно использовать данный продукт, но запрещает удалять уведомления об авторских правах.',
        'path' => 'price',
        'category' => 'news',
        'date' => '02.12.2018',
    ],
    [
        'title' => 'Осуществление технической поддержки скрипта',
        'content' => 'Мы всегда рады помочь новичкам в работе с нашими продуктами, оставляйте свои Issue в Gitlab или пишите вопросы на почту!',
        'path' => 'price',
        'category' => 'news',
        'date' => '01.12.2018',
    ],
];
foreach ($nt as $item) {
    $category = new \News\Model\News($item);
    $category->save();
}

rename(__FILE__, __DIR__ . '/setup.' . md5(time()) . '.php');
