(function ($) {
    "use strict";
    
    // popover
    $('[data-toggle="popover"]')[0] && $('[data-toggle="popover"]').popover();
    
    // editor
    let $editor = $('.html-editor-cm');
    
    if ($editor.length) {
        let params = {
            codeviewFilter: false,
            codeviewIframeFilter: false,
            codeviewFilterRegex: '',
            lang: 'ru-RU',
            height: 350,
            placeholder: 'вводите текст здесь...',
            callbacks: {
                onInit: function(e) {
                    let $this = $(this);
    
                    $this.summernote('code', 'Загрузка..');
                    
                    setTimeout(() => {
                        $this.summernote('code', $this.data('value'));
                        
                        setInterval(() => {$this.val($this.summernote('code'));}, 250);
                    }, 500);
                },
                onImageUpload: function (files) {
                    let $this = $(this),
                        data = new FormData();
                    data.append("image", files[0]);
                    
                    $.ajax({
                        url: '/file/upload',
                        cache: false,
                        contentType: false,
                        processData: false,
                        data: data,
                        type: 'post',
                        success: function (url) {
                            let image = url.image[0],
                                path = '/uploads/' + image.salt + '/' + image.name,
                                $img = $('<img>').attr('src', path);
                            
                            $this.summernote('insertNode', $img[0]);
                            $('<input type="hidden" name="file[]">').attr('value', image.id).appendTo($('form'));
                        },
                        error: function (data) {
                            console.log(data);
                        }
                    });
                }
            },
            codemirror: {
                theme: 'monokai',
                lineNumbers: true,
            }
        };
        
        if ($editor.data('code') === true) {
            params.toolbar = [["view", ["codeview"]]];
        }
        
        $editor.summernote(params);
        
        setTimeout(() => {
            if ($editor.data('code') === true) {
                $editor.next().find('.btn-codeview').click();
            }
            
            if ($editor.data('btn-disable') === true) {
                $editor.next().find('.note-toolbar').remove();
            }
        }, 50);
    }
    
    // dropzone
    Dropzone.autoDiscover = false;
    
    // // upload from news
    // $('#upload-news').dropzone({
    //     url: "/file/upload",
    //     paramName: 'file',
    //     maxFilesize: 3,
    //     addRemoveLinks: true,
    //     dictRemoveFile: 'Убрать',
    //     parallelUploads: 1,
    //     success: function (file) {
    //         let image = JSON.parse(file.xhr.response).file[0],
    //             path = '/uploads/' + image.salt + '/' + image.name,
    //             $img = $('<img>').attr('src', path);
    //
    //         $('<input type="hidden" name="file[]">').attr('value', image.id).appendTo(file.previewElement);
    //
    //         swal({
    //             title: 'Изображение загружено',
    //             text: 'Выберите в какое поле вставить изображение?',
    //             icon: 'success',
    //             buttons: {
    //                 description: {
    //                     text: 'Описание',
    //                 },
    //                 content: {
    //                     text: 'Содержимое',
    //                 },
    //                 cancel: 'Отмена',
    //             },
    //         }).then((value) => {
    //             switch (value) {
    //                 case 'description':
    //                     $editor.first().summernote('insertNode', $img[0]);
    //                     break;
    //
    //                 case 'content':
    //                     $editor.last().summernote('insertNode', $img[0]);
    //                     break;
    //             }
    //         });
    //     }
    // });
    
    // upload from files
    $('#upload').dropzone({
        url: "/file/upload",
        paramName: 'file',
        maxFilesize: 3
    });
    
})(jQuery);
