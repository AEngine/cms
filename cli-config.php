<?php

require_once __DIR__ . '/engine/init.php';

return \Doctrine\ORM\Tools\Console\ConsoleRunner::createHelperSet($app->entityManager());
