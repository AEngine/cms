<?php

namespace TradeMaster {

    use AEngine\Orchid\App;
    use AEngine\Orchid\Http\Request;
    use AEngine\Orchid\Http\Response;
    use AEngine\Orchid\Support\Crypta;
    use AEngine\Orchid\Support\Session;
    use TradeMaster\Collection\Category as CollectionCategory;
    use TradeMaster\Collection\Item as CollectionItem;
    use TradeMaster\Model\Item as ModelItem;
    use Twig\TwigFunction as Twig_SimpleFunction;

    class ModuleTradeMaster extends \CMSModule
    {
        public static function onAfterSetup(App $app)
        {
            // after setup
            $catalogPath = $app->storage->value('module_trademaster', 'catalog');

            // страница категорий товаров
            static::addRoute('get', '#' . str_replace('/', '\/', $catalogPath) . '(?>\/(?>(?<offset>\d+(?=$))|(?<link>[\d\w\-]*)))*#', function (Request $req, Response $res, $args) use ($catalogPath) {
                $default = [
                    'link' => '',
                    'offset' => 0,
                    'proizv' => $req->getQueryParam('proizv', ''),
                    'sort' => (array)$req->getQueryParam('sort', []),
                ];
                $data = array_merge($default, $args[':capture']);
                $catalog = CollectionCategory::fetch([], ['poryadok' => 'asc']);

                // ищем товар
                if ($data['link']) {
                    $item = ModelItem::fetch([
                        'link' => $data['link'],
                    ]);

                    if ($item) {
                        if ($item->vStrukture > 0) {
                            // todo 404 function
                            if ($item->path() !== $req->getUri()->getPath()) {
                                return $res->withStatus(404)->write(
                                    $this->view->fetchFromString('{% extends ["p404.html", "p404.twig", "cup/p404.html", "cup/p404.twig"] %}')
                                );
                            }
                        }

                        // сначала генерируем страницу
                        $rendered = render($req, $res, 'trademaster/item.html', [
                            'catalog' => $catalog,
                            'item' => $item,
                            'path' => explode('/', trim(str_replace('/catalog', '', $req->getUri()->getPath()), '/')),
                        ]);

                        // потом давляем просмотренные товары
                        if ($this->storage->value('module_trademaster', 'reviewed')) {
                            $buf = collect(json_decode(Session::get('trademaster_items', '{}'), true));
                            $buf = $buf->set(null, $item->idTovar)->slice(-5, 5)->unique()->toJson();
                            Session::set('trademaster_items', $buf);
                        }

                        // отдаем
                        return $rendered;
                    }
                }

                $criteria = []; // критерии выборки

                // ищем категорию
                if ($data['link']) {
                    $category = $catalog->where('link', '=', $data['link'])->first();

                    // здесь проверяем что категория существует
                    // todo 404 function
                    if ($category === null) {
                        return $res->withStatus(404)->write(
                            $this->view->fetchFromString('{% extends ["p404.html", "p404.twig", "cup/p404.html", "cup/p404.twig"] %}')
                        );
                    }

                    $path = $category->path();

                    // путь с страницей
                    if (is_string($data['offset']) && $data['offset'] != '') { $path .= '/' . $data['offset']; }

                    // здесь проверяем путь к категории и текущий путь
                    // todo 404 function
                    if ($path !== $req->getUri()->getPath()) {
                        return $res->withStatus(404)->write(
                            $this->view->fetchFromString('{% extends ["p404.html", "p404.twig", "cup/p404.html", "cup/p404.twig"] %}')
                        );
                    }

                    $criteria['vStrukture'] = $category->idZvena;
                }

                // производитель
                if ($data['proizv']) {
                    $criteria['proizv'] = $data['proizv'];
                }

                // защищаем сортировку от подмены значения
                foreach ($data['sort'] as $key => &$variant) {
                    if (!in_array(strtolower($variant), ['asc', 'desc'])) {
                        unset($data['sort'][$key]);
                    }
                }

                return render($req, $res, 'trademaster/catalog.html', [
                    'catalog' => $catalog,
                    'limit' => $this->storage->value('module_trademaster', 'pagination'),
                    'offset' => $data['offset'],
                    'items' => CollectionItem::fetch(
                        $criteria,
                        $data['sort'],
                        $this->storage->value('module_trademaster', 'pagination'),
                        $data['offset'] * $this->storage->value('module_trademaster', 'pagination')
                    )->calcRowsCount(),
                    'path' => explode('/', trim(str_replace($catalogPath, '', $req->getUri()->getPath()), '/')),
                    'link' => $data['link'],
                ]);
            });

            // страница корзины
            static::addRoute(['get', 'post'], '/cart', function (Request $req, Response $res) {
                if ($req->isPost()) {
                    $data = [
                        'name' => $req->getParam('name', ''),
                        'address' => $req->getParam('address', ''),
                        'phone' => $req->getParam('phone', ''),
                        'email' => $req->getParam('email', ''),
                        'comment' => $req->getParam('company', '') .' '.$req->getParam('inn', '').' '.$req->getParam('comment', '') ,
                        'coupon' => $req->getParam('coupon', ''),
                        'delivery' => $req->getParam('delivery', ''),
                        'shipping' => $req->getParam('shipping', 0),
                        'items' => [],
                    ];

                    $total = 0;

                    for ($i = 1; $i <= $req->getParam('itemCount', 0); $i++) {
                        $item = [
                            'id' => '',
                            'name' => $req->getParam('item_name_' . $i, ''),
                            'quantity' => $req->getParam('item_quantity_' . $i, 0),
                            'price' => $req->getParam('item_price_' . $i, 0),
                            'options' => $req->getParam('item_options_' . $i, ''),
                        ];

                        $total += ($item['quantity'] * $item['price']);

                        if ($item['options']) {
                            $item['options'] = explode(', ', $item['options']);

                            if ($item['options'] && is_array($item['options'])) {
                                $options = [];
                                foreach ($item['options'] as $el) {
                                    $el = explode(': ', $el);

                                    switch ($el[0]) {
                                        case 'product':
                                            $item['id'] = $el[1];
                                            break;
                                        default:
                                            $options[$el[0]] = $el[1];
                                    }
                                }
                                $item['options'] = $options;
                            }
                        }

                        $data['items'][] = $item;
                    }

                    // расчет доставки
                    foreach (Order::getShipping() as $variant) {
                        if ($total >= $variant['total']) {
                            $data['items']['shipping'] = [
                                'id' => $variant['id'],
                                'name' => 'Доставка',
                                'quantity' => 1,
                                'price' => $variant['price'],
                            ];
                        }
                    }

                    $result = ModuleTradeMaster::api([
                        'method' => 'POST',
                        'endpoint' => 'order/cart/anonym',
                        'params' => [
                            'sklad' => $this->storage->value('module_trademaster', 'storage'),
                            'urlico' => $this->storage->value('module_trademaster', 'legal'),
                            'ds' => $this->storage->value('module_trademaster', 'checkout'),
                            'kontragent' => $this->storage->value('module_trademaster', 'contractor'),
                            'shema' => $this->storage->value('module_trademaster', 'scheme'),
                            'valuta' => $this->storage->value('module_trademaster', 'api_currency'),
                            'userID' => $this->storage->value('module_trademaster', 'user_id'),
                            'nameKontakt' => $data['name'],
                            'adresKontakt' => $data['address'],
                            'telefonKontakt' => $data['phone'],
                            'other1Kontakt' => $data['email'],
                            'dateDost' => $data['delivery'],
                            'komment' => $data['comment'],
                            'tovarJson' => json_encode(array_values($data['items']), JSON_UNESCAPED_UNICODE),
                        ],
                    ]);

                    if (!empty($result['nomerZakaza'])) {

                        // письмо клиенту
                        if ($data['email']) {
                            sendMail([
                                'to' => [$data['email'] => $data['name']],
                                'body' => preg_replace(
                                    ['/\>[^\S ]+/s', '/[^\S ]+\</s', '/(\s)+/s', '/<!--(.|\s)*?-->/'],
                                    ['>', '<', '\\1', ''],
                                    $this->view->fetch('trademaster/mail/cart.html', [
                                        'order' => Order::getInfo(['nomer' => $result['nomerZakaza']]),
                                    ])
                                ),
                                'isHtml' => true,
                            ]);
                        }

                        // письмо администратору
                        if ($email = $this->storage->value('module_trademaster', 'email')) {
                            sendMail([
                                'to' => [$email => 'Administrator'],
                                'body' => preg_replace(
                                    ['/\>[^\S ]+/s', '/[^\S ]+\</s', '/(\s)+/s', '/<!--(.|\s)*?-->/'],
                                    ['>', '<', '\\1', ''],
                                    $this->view->fetch('trademaster/mail/cart.html', [
                                        'order' => Order::getInfo(['nomer' => $result['nomerZakaza']]),
                                    ])
                                ),
                                'isHtml' => true,
                            ]);
                        }

                        return $res->withAddedHeader('Location', '/cart/done?order=' . Crypta::encrypt($result['nomerZakaza']));
                    }
                }

                return render($req, $res, 'trademaster/cart.html');
            });

            // страница завершения корзины
            static::addRoute('get', '/cart/done', function (Request $req, Response $res) {
                return render($req, $res, 'trademaster/cart/done.html', [
                    'order' => Order::getInfo(['nomer' => Crypta::decrypt($req->getParam('order', ''))])
                ]);
            });

            // поиск в яндексе
            static::addRoute(['get', 'post'], '#\/search\/(?<text>[\w\%]*)(?:\/(?<page>[0-9]*)|)#', function (Request $req, Response $res, $args) {
                if ($req->isXhr()) {
                    $default = [
                        'text' => '',
                        'page' => 0,
                    ];
                    $data = array_merge($default, $req->getParams(), $args[':capture']);
                    $data['text'] = urldecode($data['text']);
                    $data['page'] = +$data['page'];

                    return $res
                        ->withAddedHeader('Content-Type', 'application/json')
                        ->write(
                            tm_yandex_search($data)
                        );
                }

                return $res->withStatus(404);
            });
        }

        /**
         * Поиск в яндексе
         *
         * @param array $data
         * @param bool  $json
         *
         * @return false|array|string
         */
        public static function searchYandex(array $data = [], $json = true)
        {
            $default = [
                'text' => null,
                'page' => 0,
                'per_page' => app()->storage->value('module_trademaster', 'yandex_search_per_page'),
                'how' => null,
                'price_low' => null,
                'price_high' => null,
                'category_id' => null,
                'available' => 'false',
            ];
            $important = [
                'apikey' => app()->storage->value('module_trademaster', 'yandex_search_api'),
                'searchid' => app()->storage->value('module_trademaster', 'yandex_search_id'),
            ];
            $data = array_intersect_key(
                array_merge($default, $data, $important),
                array_merge($default, $important)
            );

            $result = file_get_contents('https://catalogapi.site.yandex.net/v1.0?' . http_build_query($data));

            return $json ? $result : json_decode($result, true);
        }

        /**
         * Обращение к внешней базе данных
         *
         * @param array $data
         *
         * @return mixed
         */
        public static function api(array $data = [])
        {
            $app = app();
            $default = [
                'endpoint' => '',
                'params' => [],
                'method' => 'GET',
            ];
            $data = array_merge($default, $data);
            $data['method'] = strtoupper($data['method']);

            $pathParts = [$app->storage->value('module_trademaster', 'api_host'), 'v' . $app->storage->value('module_trademaster', 'api_version'), $data['endpoint']];

            if ($data['method'] == "GET") {
                $data['params']['apikey'] = $app->storage->value('module_trademaster', 'api_key');
                $path = implode('/', $pathParts) . '?' . http_build_query($data['params']);
                $result = file_get_contents($path);
            } else {
                $path = implode('/', $pathParts) . '?' . http_build_query(['apikey' => $app->storage->value('module_trademaster', 'api_key')]);
                $result = file_get_contents($path, false, stream_context_create([
                    'http' =>
                        [
                            'method' => 'POST',
                            'header' => 'Content-type: application/x-www-form-urlencoded',
                            'content' => http_build_query($data['params']),
                            'timeout' => 60,
                        ],
                ]));
            }

            return json_decode($result, true);
        }

        // настройки модуля
        public static function getSettings()
        {
            return [
                'title' => 'TradeMaster',
                'description' => 'Интеграция с системой ведения управленческого учета',
                'params' => [
                    'reviewed' => [
                        'title' => 'Просмотренные товары',
                        'description' => 'Дает возможность отображать историю просмотренных посетителем товаров',
                        'type' => 'switch',
                        'value' => 'off',
                    ],
                    'pagination' => [
                        'title' => 'На страницу',
                        'description' => 'Количество товаров отображаемые на странице',
                        'value' => 30,
                    ],
                    'catalog' => [
                        'title' => 'Адрес каталога',
                        'description' => 'Путь к каталогу (не рекомендуется изменять)',
                        'value' => '/catalog',
                        'placeholder' => '/catalog',
                    ],
                    'shipping' => [
                        'title' => 'Доставка',
                        'description' => 'В данном поле указывается: ID товара доставки, стоимость доставки, сумма корзины. Например: 10,550,0;11,350,5000;12,0,10000;..',
                        'type' => 'textarea',
                        'value' => '',
                    ],
                    'currency' => [
                        'title' => 'Валюта',
                        'description' => 'Отображается на страницах сайта',
                        'value' => 'RUB',
                    ],

                    // настройки уведомлений
                    ['title' => 'Уведомление о заказе', 'type' => static::PARAM_TYPE_SEPARATOR],
                    'email' => [
                        'title' => 'E-Mail администратора',
                        'description' => 'На этот адрес будет высылаться копия письма подтверждающая заказ клиентом',
                        'value' => '',
                    ],

                    // настройки обращения к tm
                    ['title' => 'Параметры TradeMaster API', 'type' => static::PARAM_TYPE_SEPARATOR],
                    'api_host' => [
                        'title' => 'API Host',
                        'description' => 'Хост API сервера',
                        'value' => 'https://api.trademaster.pro',
                        'editable' => false,
                    ],
                    'api_version' => [
                        'title' => 'API Version',
                        'description' => 'Версия API сервера',
                        'value' => 2,
                        'editable' => false,
                    ],
                    'api_key' => [
                        'title' => 'API Key',
                        'description' => 'Введите полученный вами ключ',
                        'value' => 'pJC6B1Xb3Iv4lBdv0eZ2Mq763z8RnkfqYK4bJC9H',
                    ],
                    'api_currency' => [
                        'title' => 'API Currency',
                        'description' => 'Валюта отправляемая по API',
                        'value' => 'RUB',
                    ],
                    'cache_host' => [
                        'title' => 'Cache host',
                        'description' => 'Хост кеш файлов',
                        'value' => 'trademaster.pro',
                    ],
                    'cache_folder' => [
                        'title' => 'Cache folder',
                        'description' => 'Папка кеш файлов',
                        'value' => 'demo',
                    ],

                    // настройки tm
                    ['type' => static::PARAM_TYPE_SEPARATOR],
                    'struct' => [
                        'title' => 'Struct',
                        'description' => '',
                        'value' => 0,
                    ],
                    'storage' => [
                        'title' => 'Storage',
                        'description' => '',
                        'value' => 1,
                    ],
                    'legal' => [
                        'title' => 'Legal',
                        'description' => '',
                        'value' => 1,
                    ],
                    'checkout' => [
                        'title' => 'Checkout',
                        'description' => '',
                        'value' => 1,
                    ],
                    'contractor' => [
                        'title' => 'Contractor',
                        'description' => '',
                        'value' => 2,
                    ],
                    'scheme' => [
                        'title' => 'Scheme',
                        'description' => '',
                        'value' => 6,
                    ],
                    'user_id' => [
                        'title' => 'User ID',
                        'description' => '',
                        'value' => 54,
                    ],

                    // настройки yml
                    ['title' => 'Параметры Yandex Market', 'type' => static::PARAM_TYPE_SEPARATOR],
                    'yml_shop' => [
                        'title' => 'Название магазина',
                        'description' => 'Название будет использовано в сгенерированном YML файле',
                        'value' => 'Демо интернет-магазин на TradeMaster c OmniSITE',
                    ],
                    'yml_company' => [
                        'title' => 'Название компании',
                        'description' => 'Название будет использовано в сгенерированном YML файле',
                        'value' => 'ООО "Рога и Копыта"',
                    ],
                    'yml_delivery' => [
                        'title' => 'Стоимость доставки',
                        'description' => 'Стоимость доставки будет использована в сгенерированном YML файле',
                        'value' => 0,
                    ],
                    'yml_currency' => [
                        'title' => 'Валюта',
                        'description' => 'Валюта будет использована в сгенерированном YML файле',
                        'value' => 'RUB',
                    ],

                    // настройки yml
                    ['title' => 'Параметры Google Merchant Feed', 'type' => static::PARAM_TYPE_SEPARATOR],
                    'gmf_shop' => [
                        'title' => 'Название магазина',
                        'description' => 'Название будет использовано в сгенерированном GMF файле',
                        'value' => 'Демо интернет-магазин на TradeMaster c OmniSITE',
                    ],
                    'gmf_description' => [
                        'title' => 'Описание магазина',
                        'description' => 'Описание будет использовано в сгенерированном GMF файле',
                        'value' => '',
                    ],
                    'gmf_currency' => [
                        'title' => 'Валюта',
                        'description' => 'Валюта будет использована в сгенерированном GMF файле',
                        'value' => 'RUB',
                    ],

                    // yandex search
                    ['title' => 'Яндекс.Поиск', 'type' => static::PARAM_TYPE_SEPARATOR],
                    'yandex_search_api' => [
                        'title' => 'Ключ API',
                        'description' => 'Ключ доступа к сервису поиска',
                        'placeholder' => '00000000-1111-2222-3333-444444444444',
                    ],
                    'yandex_search_id' => [
                        'title' => 'Идентификатор поиска',
                        'description' => '',
                        'placeholder' => '1234567',
                    ],
                    'yandex_search_per_page' => [
                        'title' => 'На страницу',
                        'description' => 'Количество результатов на страницу',
                        'placeholder' => '10',
                        'value' => '10',
                    ],
                ]
            ];
        }

        // функции модуля
        public static function getFunction()
        {
            return [
                new Twig_SimpleFunction('tm_api', function ($endpoint, array $params = [], $method = 'GET') {
                    return ModuleTradeMaster::api([
                        'endpoint' => $endpoint,
                        'params' => $params,
                        'method' => $method,
                    ]);
                }),
                new Twig_SimpleFunction('tm_catalog_category', function (array $criteria = [], array $orderBy = null, $limit = null, $offset = null) {
                    foreach ($criteria as $key => $value) {
                        if (blank($value)) {
                            unset($criteria[$key]);
                        }
                    }
                    if (!$orderBy) {
                        $orderBy = ['poryadok' => 'asc'];
                    }

                    return CollectionCategory::fetch($criteria, $orderBy, $limit, $offset);
                }),
                new Twig_SimpleFunction('tm_catalog_items', function (array $criteria = [], array $orderBy = null, $limit = null, $offset = null) {
                    foreach ($criteria as $key => $value) {
                        if (blank($value)) {
                            unset($criteria[$key]);
                        }
                    }

                    return CollectionItem::fetch($criteria, $orderBy, $limit, $offset);
                }),
                new Twig_SimpleFunction('tm_catalog_item', function (array $criteria = [], array $orderBy = null) {
                    foreach ($criteria as $key => $value) {
                        if (blank($value)) {
                            unset($criteria[$key]);
                        }
                    }

                    return ModelItem::fetch($criteria, $orderBy);
                }),
                new Twig_SimpleFunction('tm_order_info', function ($nomer, $retJSON = true) {
                    $order = Order::getInfo(['nomer' => $nomer]);

                    return $retJSON ? json_encode($order, JSON_UNESCAPED_UNICODE) : $order;
                }),
                new Twig_SimpleFunction('tm_order_shipping', function ($retJSON = true) {
                    $shipping = Order::getShipping();

                    return $retJSON ? json_encode($shipping, JSON_UNESCAPED_UNICODE) : $shipping;
                }),
                new Twig_SimpleFunction('tm_reviewed', function () {
                    $ids = json_decode(Session::get('trademaster_items', '{}'), true);

                    if ($ids && app()->storage->value('module_trademaster', 'reviewed') == 'on') {
                        return CollectionItem::fetch(['idTovar' => $ids], null);
                    }

                    return false;
                }),
            ];
        }

        public static function getBin()
        {
            return [
                [
                    'title' => 'Обновить категории',
                    'script' => \TradeMaster\Bin\UpdateCatalog::class,
                ],
                [
                    'title' => 'Обновить товары',
                    'script' => \TradeMaster\Bin\UpdateItems::class,
                ],
                'sitemap' => [
                    'title' => 'Обновить карту сайта',
                    'script' => \TradeMaster\Bin\UpdateSitemap::class,
                    'button_variant' => 'info',
                ],
                'gmf' => [
                    'title' => 'Обновить Google Merchant файл',
                    'script' => \TradeMaster\Bin\UpdateGoogleFeed::class,
                ],
                'yml' => [
                    'title' => 'Обновить Yandex Market файл',
                    'script' => \TradeMaster\Bin\UpdateYML::class,
                ],
            ];
        }

        public static function getStatistic()
        {
            return [
                [
                    'title' => 'TM Категорий',
                    'value' => CollectionCategory::fetch()->count(),
                ],
                [
                    'title' => 'TM Товаров',
                    'value' => CollectionItem::fetch()->count(),
                ],
            ];
        }
    }
}

namespace {

    use TradeMaster\ModuleTradeMaster;

    /**
     * Возвращет путь для файла из TradeMaster
     *
     * @param $foto
     *
     * @return string
     * @throws ReflectionException
     */
    function tm_get_foto_path($foto)
    {
        $app = app();

        return 'https://' .
            $app->storage->value('module_trademaster', 'cache_host') .
            '/tradeMasterImages/' .
            $app->storage->value('module_trademaster', 'cache_folder') .
            '/' .
            str_replace(' ', '%20', trim($foto));
    }

    /**
     * @param array $data
     * @param bool  $json
     *
     * @return false|array|string
     */
    function tm_yandex_search(array $data = [], $json = true)
    {
        return ModuleTradeMaster::searchYandex($data, $json);
    }
}
