<?php

namespace TradeMaster;

class Order
{
    public static function getInfo(array $data = [])
    {
        $default = [
            'nomer' => '',
            'vid' => 12,
        ];
        $data = array_merge($default, $data);

        $result = ModuleTradeMaster::api([
            'endpoint' => 'order/info',
            'params' => [
                'nomer' => $data['nomer'],
            ],
        ]);

        $order = ['items' => [], 'service' => [], 'total' => 0];

        if ($result) {
            foreach ($result as $item) {
                $tovar = [];

                foreach ($item as $key => $value) {
                    if (in_array($key, [
                        'tovarName', 'opisanie', 'artikul', 'edIzmer',
                        'ves', 'price', 'opt_price', 'sebestomost', 'proizv', 'kolvo', 'summa',
                        'ind1', 'ind2', 'ind3', 'foto_small', 'foto_big', 'postav', 'postavName',
                        'aktualnost', 'flag', 'flag1',
                    ])) {
                        $tovar[$key] = $value;
                    } else {
                        $order[$key] = $value;
                    }
                }

                // определяем товар и услуги
                switch (true) {
                    // услуга
                    case ((strtolower($tovar['edIzmer']) == 'услуга' || strtolower($tovar['artikul']) == 'услуга') || (!(int)$tovar['ves'] && !empty($tovar['price']))):
                        $order['service'][] = $tovar;
                        break;

                    // товар
                    case (!empty($tovar['artikul']) || (!empty($tovar['ves']) && !empty($tovar['price']))):
                        $order['items'][] = $tovar;
                        break;
                }

                // общая сумма
                $order['total'] += $tovar['summa'];
            }
        }


        return $result ? $order : false;
    }

    // параметры доставки
    public static function getShipping()
    {
        $shipping = app()->storage->value('module_trademaster', 'shipping', null);

        if ($shipping) {
            $variants = [];

            foreach (explode(';', $shipping) as $variant) {
                if ($variant) {
                    $buf = explode(',', $variant);

                    $variants[] = [
                        'id' => (int)($buf[0] ?? 0),
                        'price' => (int)($buf[1] ?? 0),
                        'total' => (int)($buf[2] ?? 0),
                    ];
                }
            }

            // сортируем по стоимости корзины
            usort($variants, function ($a, $b) {
                return $a['total'] > $b['total'];
            });

            return $variants;
        }

        return [];
    }
}
