<?php

namespace TradeMaster\Bin;

use InvalidArgumentException;
use samdark\sitemap\Sitemap;
use TradeMaster\Collection\Category as CollectionCategory;
use TradeMaster\Collection\Item as CollectionItem;
use TradeMaster\Model\Category as ModelCategory;

class UpdateSitemap extends \Bin\UpdateSitemap
{
    public static function exec()
    {
        $app = app();
        $baseurl = rtrim($app->storage->value('module_system', 'homepage'), '/');
        $catalogPath = $app->storage->value('module_trademaster', 'catalog');

        /**
         * @var Sitemap $map
         */
        $map = parent::exec();

        $map->addItem($baseurl . $catalogPath, time(), Sitemap::WEEKLY, 0.5);

        $categories = CollectionCategory::fetch();

        /**
         * @var ModelCategory $category
         */
        foreach ($categories as $category) {
            $parentUrl = '/';

            if ($category) {
                $parentUrl = $category->getParents($categories);
                $parentUrl = '/' . $parentUrl->pluck('link')->implode('');
            }

            $path = rtrim($baseurl . $catalogPath . $parentUrl, '/');
            try {
                $map->addItem($path, time(), Sitemap::WEEKLY, 0.6);
            } catch (InvalidArgumentException $e) {
                $app->logger->warning('Sitemap: category with Id:' . $category->idZvena . ' have wrong url. Skipped', ['url' => $path]);
            }
        }

        foreach (CollectionItem::fetch()->whereNotIn('vStrukture', 0) as $item) {
            /**
             * @var ModelCategory $category
             */
            $category = $categories->where('idZvena', '', $item->vStrukture)->first();
            $parentUrl = '/';

            if ($category) {
                $parentUrl = $category->getParents($categories);
                $parentUrl = '/' . $parentUrl->pluck('link')->implode('');
            }

            $path = trim($baseurl . $catalogPath . $parentUrl . $item->link);
            try {
                $map->addItem($path, time(), Sitemap::WEEKLY, 0.8);
            } catch (InvalidArgumentException $e) {
                $app->logger->warning('Sitemap: item with Id:' . $item->idTovar . ' have wrong url. Skipped', ['url' => $path]);
            }
        }

        $map->write();

        $app->logger->info('Sitemap extended: created');
    }
}
