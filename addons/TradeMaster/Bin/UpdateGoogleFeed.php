<?php

namespace TradeMaster\Bin;

use TradeMaster\Collection\Item as CollectionItem;
use Vitalybaev\GoogleMerchant\Feed;
use Vitalybaev\GoogleMerchant\Product;
use TradeMaster\Collection\Category as CollectionCategory;
use TradeMaster\Model\Category as ModelCategory;
use TradeMaster\Model\Item as ModelItem;

class UpdateGoogleFeed
{
    public static function exec()
    {
        $app = app();
        $baseurl = rtrim($app->storage->value('module_system', 'homepage'), '/');
        $catalogPath = $app->storage->value('module_trademaster', 'catalog');

        $feed = new Feed(
            $app->storage->value('module_trademaster', 'gmf_shop'),
            $baseurl,
            $app->storage->value('module_trademaster', 'gmf_description')
        );

        $categories = CollectionCategory::fetch();

        /**
         * @var ModelItem $item
         */
        foreach (CollectionItem::fetch()->whereNotIn('vStrukture', 0) as $item) {
            /**
             * @var ModelCategory $category
             */
            $category = $categories->where('idZvena', '', $item->vStrukture)->first();
            $parentUrl = '/';

            if ($category) {
                $parentUrl = $category->getParents($categories);
                $parentUrl = '/' . $parentUrl->pluck('link')->implode('');
            }

            $buf = new Product();

            $buf->setId($item->idTovar);
            $buf->setTitle($item->name);
            $buf->setDescription($item->opisanie);
            $buf->setAttribute('link', $baseurl . $catalogPath . $parentUrl . $item->link);

            if ($item->foto) {
                $images = $item->getImages();
                if ($images->count()) {
                    $image = $images->first();
                    $buf->setAttribute('image_link', $baseurl . '/uploads/' . $image->salt . '/' . $image->name);
                }
            } else {
                $images = $category->getImages();
                if ($images->count()) {
                    $image = $images->first();
                    $buf->setAttribute('image_link', $baseurl . '/uploads/' . $image->salt . '/' . $image->name);
                }
            }

            $buf->setPrice("{$item->price} {$app->storage->value('module_trademaster', 'gmf_currency')}");
            $buf->setCondition(Product\Condition::NEW);
            $buf->setAttribute('availability', 'available for order', false);

            if ($item->price > 0) {
                $feed->addProduct($buf);
            }
        }

        file_put_contents($app->getBaseDir() . '/gmf.xml', $feed->build());
    }
}
