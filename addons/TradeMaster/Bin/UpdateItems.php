<?php

namespace TradeMaster\Bin;

use Interfaces\BinInterface;
use TradeMaster\Catalog;
use TradeMaster\Collection\Item as CollectionItem;

class UpdateItems implements BinInterface
{
    public static function exec()
    {
        /*$uploads = app()->path->get('uploads:');

        foreach (CollectionItem::fetch() as $item) {
            foreach ($item->getImages() as $image) {
                $folder = $uploads . $image->salt;
                $file = $folder . '/' . $image->name;

                if (file_exists($file)) {
                    unlink($file);
                    rmdir($folder);
                }

                $image->remove();
            }
        }*/

        Catalog::updateItems();
    }
}
