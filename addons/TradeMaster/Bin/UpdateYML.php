<?php

namespace TradeMaster\Bin;

use Interfaces\BinInterface;
use TradeMaster\Collection\Category as CollectionCategory;
use TradeMaster\Collection\Item as CollectionItem;
use TradeMaster\Model\Category as ModelCategory;
use TradeMaster\Model\Item as ModelItem;
use TradeMaster\Yml;
use Upload\Model\File as ModelFile;

class UpdateYML implements BinInterface
{
    public static function exec()
    {
        $app = app();
        $baseurl = rtrim($app->storage->value('module_system', 'homepage'), '/');
        $catalogPath = $app->storage->value('module_trademaster', 'catalog');

        $yml = new Yml();

        $yml->set_shop(
            $app->storage->value('module_trademaster', 'yml_shop'),
            $app->storage->value('module_trademaster', 'yml_company'),
            $baseurl
        );
        $yml->set_delivery($app->storage->value('module_trademaster', 'yml_delivery'));
        $yml->add_currency($app->storage->value('module_trademaster', 'yml_currency'), 1);

        $categories = CollectionCategory::fetch();

        // обход всех категорий
        foreach ($categories as $category) {
            $yml->add_category($category->nameZvena, $category->idZvena, $category->idParent ? $category->idParent : -1);
        }

        /**
         * @var ModelItem $item
         */
        foreach (CollectionItem::fetch()->whereNotIn('vStrukture', 0) as $item) {
            /**
             * @var ModelCategory $category
             */
            $category = $categories->where('idZvena', '', $item->vStrukture)->first();
            $parentUrl = '/';

            if ($category) {
                $parentUrl = $category->getParents($categories);
                $parentUrl = '/' . $parentUrl->pluck('link')->implode('');
            }

            if ($item->price > 0) {
                $tovar = [
                    'price' => $item->price,
                    'currencyId' => $app->storage->value('module_trademaster', 'yml_currency'),
                    'categoryId' => $item->vStrukture,
                    'country_of_origin' => $item->strana,
                    'vendor' => $item->proizv,
                    'vendorCode' => $item->artikul,
                    'name' => $item->name,
                    'description' => $item->opisanieDop ? $item->opisanieDop : $item->opisanie,
                    'url' => $baseurl . $catalogPath . $parentUrl . $item->link,
                    'delivery' => 'true',
                    'store' => 'false',
                    'pickup' => 'true',
                ];

                if ($item->foto) {
                    $images = $item->getImages();

                    if ($images->isNotEmpty()) {
                        /**
                         * @var ModelFile $image
                         */
                        $image = $images->first();
                        $tovar['picture'] = $baseurl . '/uploads/' . $image->salt . '/' . $image->name;
                    }
                } else {
                    $images = $category->getImages();

                    if ($images->isNotEmpty()) {
                        /**
                         * @var ModelFile $image
                         */
                        $image = $images->first();
                        $tovar['picture'] = $baseurl . '/uploads/' . $image->salt . '/' . $image->name;
                    }
                }

                $yml->add_offer($item->idTovar, $tovar, true);
            }
        }

        file_put_contents($app->getBaseDir() . '/yml.xml', $yml->get_xml());
    }
}
