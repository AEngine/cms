<?php

namespace TradeMaster;

use RuntimeException;
use TradeMaster\Collection\Category as CollectionCategory;
use TradeMaster\Collection\Item as CollectionItem;
use TradeMaster\Model\Category as ModelCategory;
use TradeMaster\Model\Item as ModelItem;

class Catalog
{
    /**
     * Обновление списка категорий
     *
     * @return bool
     * @throws \Doctrine\DBAL\DBALException
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     * @throws \ReflectionException
     */
    public static function updateCategories()
    {
        $app = app();
        $app->logger->info('TradeMaster: update categories');

        /**
         * @var \Doctrine\ORM\EntityManager $em
         */
        $em = $app->entityManager;
        $connection = $em->getConnection();
        $platform = $connection->getDatabasePlatform();

        // удаляем старые данные
        $connection->executeUpdate($platform->getTruncateTableSQL('TradeMaster_Category', true));

        // запрашиваем новые данные о категориях
        $result = ModuleTradeMaster::api([
            'endpoint' => 'catalog/list',
        ]);
        if (!$result) {
            throw new RuntimeException('Не удалось получить категории');
        }
        $categories = new CollectionCategory($result);

        foreach ($categories as $model) {
            $model = new ModelCategory($model);
            $model->processImages();

            if ($model->filter() === true) {
                $model->save();
                $em->clear();

                continue;
            }

            throw new RuntimeException('Ошибка при проверке категорий');
        }

        return true;
    }

    /**
     * Обновление списка товаров
     *
     * @return bool
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     * @throws \Doctrine\DBAL\DBALException
     * @throws \ReflectionException
     */
    public static function updateItems()
    {
        $app = app();
        $app->logger->info('TradeMaster: update items');

        /**
         * @var \Doctrine\ORM\EntityManager $em
         */
        $em = $app->entityManager;
        $connection = $em->getConnection();
        $platform = $connection->getDatabasePlatform();

        // удаляем старые данные
        $connection->executeUpdate($platform->getTruncateTableSQL('TradeMaster_Item', true));

        $result = ModuleTradeMaster::api([
            'endpoint' => 'item/count',
        ]);
        if (!$result) {
            throw new RuntimeException('Не удалось получить количество товаров');
        }
        $count = (int)$result['count'];
        $i = 0;
        $step = 500;
        $go = true;

        while ($go) {
            $result = ModuleTradeMaster::api([
                'endpoint' => 'item/list',
                'params' => [
                    'sklad' => $app->storage->value('module_trademaster', 'storage', 0),
                    'offset' => $i * $step,
                    'limit' => $step,
                ],
            ]);
            $items = new CollectionItem($result);

            foreach ($items as $model) {
                $model = new ModelItem($model);
                $model->processImages();

                if ($model->filter() === true) {
                    $model->save();
                    $em->clear();

                    continue;
                }

                throw new RuntimeException('Ошибка при проверке товара');
            }

            $go = $step * ++$i <= $count;
            usleep(500);
        }

        return true;
    }
}
