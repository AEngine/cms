<?php

namespace TradeMaster\Collection;

use Doctrine\ORM\Mapping\Cache;
use ORMCollection;

/**
 * @Cache()
 */
class Category extends ORMCollection
{
    public static $model = \TradeMaster\Model\Category::class;

    public static function fetch(array $criteria = [], array $orderBy = null, $limit = null, $offset = null)
    {
        $collection = parent::fetch($criteria, $orderBy, $limit, $offset);
        $collection = $collection->mapWithKeys(function ($item) {
            return [$item->idZvena => $item];
        });

        return $collection;
    }
}

