<?php

namespace TradeMaster\Collection;

use Doctrine\ORM\Mapping\Cache;
use ORMCollection;

/**
 * @Cache()
 */
class Item extends ORMCollection
{
    public static $model = \TradeMaster\Model\Item::class;

    public static function fetch(array $criteria = [], array $orderBy = null, $limit = null, $offset = null)
    {
        $collection = parent::fetch($criteria, $orderBy, $limit, $offset);
        $collection = $collection->mapWithKeys(function ($item) {
            return [$item->idTovar => $item];
        });

        // save params
        $collection->_criteria = $criteria;
        $collection->_orderBy = $orderBy;
        $collection->_limit = (int)$limit;
        $collection->_offset = (int)$offset;

        return $collection;
    }
}

