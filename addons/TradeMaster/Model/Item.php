<?php

namespace TradeMaster\Model;

use AEngine\Orchid\Filter as Filter;
use Doctrine\ORM\Mapping\Cache;
use Doctrine\ORM\Mapping\Column;
use Doctrine\ORM\Mapping\Entity;
use Doctrine\ORM\Mapping\Id;
use Doctrine\ORM\Mapping\Table;
use ORMFilterModel;
use Upload\Collection\File as CollectionFile;
use Upload\Model\File as ModelFile;

/**
 * @Entity
 * @Table(name="TradeMaster_Item")
 * @Cache()
 */
class Item extends ORMFilterModel
{
    /**
     * @Id
     * @Column(type="integer")
     * @Filter\Lead\Integer();
     *
     * @var int
     */
    public $idTovar;

    /**
     * @Column(type="string")
     * @Filter\Lead\Str();
     *
     * @var string
     */
    public $name;

    /**
     * @Column(type="text")
     * @Filter\Lead\Str();
     *
     * @var string
     */
    public $opisanie;

    /**
     * @Column(type="text")
     * @Filter\Lead\Str();
     *
     * @var string
     */
    public $opisanieDop;

    /**
     * @Column(type="string")
     * @Filter\Lead\Str();
     *
     * @var string
     */
    public $artikul;

    /**
     * @Column(type="string")
     * @Filter\Lead\Str();
     *
     * @var string
     */
    public $edIzmer;

    /**
     * @Column(type="integer")
     * @Filter\Lead\Integer();
     *
     * @var int
     */
    public $vStrukture;

    /**
     * @Column(type="integer")
     * @Filter\Lead\Integer();
     *
     * @var int
     */
    public $vStruktureDop;

    /**
     * @Column(type="string")
     * @Filter\Lead\Str();
     *
     * @var string
     */
    public $strihKod;

    /**
     * @Column(type="float", scale=10, precision=2)
     * @Filter\Lead\Double(2);
     *
     * @var float
     */
    public $price;

    /**
     * @Column(type="float", scale=10, precision=2, nullable=true)
     * @Filter\Lead\Double(2);
     *
     * @var float
     */
    public $priceVal;

    /**
     * @Column(type="integer")
     * @Filter\Lead\Integer();
     *
     * @var int
     */
    public $poryadok;

    /**
     * @Column(type="string", nullable=true)
     * @Filter\Lead\Str();
     *
     * @var string
     */
    public $foto;

    /**
     * @Column(type="float", scale=10, precision=2)
     * @Filter\Lead\Double(2);
     *
     * @var float
     */
    public $sebestomost;

    /**
     * @Column(type="float", scale=10, precision=2, nullable=true)
     * @Filter\Lead\Double(2);
     *
     * @var float
     */
    public $opt_price;

    /**
     * @Column(type="float", scale=10, precision=2, nullable=true)
     * @Filter\Lead\Double(2);
     *
     * @var float
     */
    public $opt_priceVal;

    /**
     * @Column(type="integer", nullable=true)
     * @Filter\Lead\Integer();
     *
     * @var int
     */
    public $publish;

    /**
     * @Column(type="string", nullable=true)
     * @Filter\Lead\Str();
     *
     * @var string
     */
    public $publish_nalich;

    /**
     * @Column(type="integer")
     * @Filter\Lead\Integer();
     *
     * @var int
     */
    public $kolvo;

    /**
     * @Column(type="text")
     * @Filter\Lead\Str();
     *
     * @var string
     */
    public $ind1;

    /**
     * @Column(type="text")
     * @Filter\Lead\Str();
     *
     * @var string
     */
    public $ind2;

    /**
     * @Column(type="text")
     * @Filter\Lead\Str();
     *
     * @var string
     */
    public $ind3;

    /**
     * @Column(type="text")
     * @Filter\Lead\Str();
     *
     * @var string
     */
    public $ind4;

    /**
     * @Column(type="text")
     * @Filter\Lead\Str();
     *
     * @var string
     */
    public $ind5;

    /**
     * @Column(type="text")
     * @Filter\Lead\Str();
     *
     * @var string
     */
    public $tags;

    /**
     * @Column(type="float", scale=10, precision=2)
     * @Filter\Lead\Double(2);
     *
     * @var float
     */
    public $ves;

    /**
     * @Column(type="text")
     * @Filter\Lead\Str();
     *
     * @var string
     */
    public $proizv;

    /**
     * @Column(type="text")
     * @Filter\Lead\Str();
     *
     * @var string
     */
    public $postav;

    /**
     * @Column(type="string", nullable=true)
     * @Filter\Lead\Str();
     *
     * @var string
     */
    public $postavName;

    /**
     * @Column(type="string")
     * @Filter\Lead\Str();
     *
     * @var string
     */
    public $strana;

    /**
     * @Column(type="integer", nullable=true)
     * @Filter\Lead\Integer();
     *
     * @var int
     */
    public $actualnost;

    /**
     * @Column(type="string", nullable=true)
     * @Filter\Lead\Str();
     *
     * @var string
     */
    public $valuta;

    /**
     * @Column(type="string", nullable=true)
     * @Filter\Lead\Str();
     *
     * @var string
     */
    public $nameZvena;

    /**
     * @Column(type="text", nullable=true)
     * @Filter\Lead\Str();
     *
     * @var string
     */
    public $fullDisplayPath;

    /**
     * @Column(type="text")
     * @Filter\Lead\Str();
     * @Filter\Lead\Regex('/\s/', '');
     *
     * @var string
     */
    public $link;

    /**
     * @Column(type="string")
     * @Filter\Lead\Str();
     *
     * @var string
     */
    public $changeDate;

    /**
     * @Column(type="array", nullable=true)
     *
     * @var array
     */
    public $image_ids = [];

    /**
     * @var CollectionFile
     */
    public $images = null;

    /**
     * Загружает изображения
     *
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     * @throws \ReflectionException
     */
    public function processImages()
    {
        // загрузка изображения
        if (blank($this->image_ids) && !blank($this->foto)) {
            $images = explode(';', $this->foto);

            foreach ($images as $image) {
                /**
                 * @var ModelFile $file
                 */
                $file = upload_from_remote(tm_get_foto_path($image));

                if ($file) {
                    $this->image_ids[] = $file->id;

                    if (str_ends_with(['jpg', 'jpeg', 'png', 'gif'], $file->name)) {
                        $file->convert();
                    }
                }
            }
        }
    }

    /**
     * Возвращает изображения
     *
     * @return \ORMCollection
     */
    public function getImages()
    {
        if (!blank($this->images)) {
            return $this->images;
        }

        $this->images = CollectionFile::fetch(['id' => $this->image_ids]);

        return $this->images;
    }

    /**
     * Возвращает первое изображение
     *
     * @param string $size
     * @param string $default
     *
     * @return mixed
     */
    public function firstImagePath($size = '', $default = '') {
        $this->getImages();

        if ($this->images->count()) {
            return $this->images->first()->path($size);
        }

        return $default;
    }

    /**
     * Возвращает путь до товара
     *
     * @param \TradeMaster\Collection\Category $catalog
     *
     * @return string
     */
    public function path($catalog = null) {
        if ($catalog === null) {
            $catalog = \TradeMaster\Collection\Category::fetch();
        }

        /** @var Category $category */
        $category = $catalog->where('idZvena', $this->vStrukture)->first();

        return $category && !$category->isEmpty() ? $category->path($catalog) . '/' . $this->link : '#';
    }
}
