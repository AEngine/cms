<?php

namespace TradeMaster\Model;

use AEngine\Orchid\Filter as Filter;
use Doctrine\ORM\Mapping\Cache;
use Doctrine\ORM\Mapping\Column;
use Doctrine\ORM\Mapping\Entity;
use Doctrine\ORM\Mapping\Id;
use Doctrine\ORM\Mapping\Table;
use ORMFilterModel;
use Upload\Collection\File as CollectionFile;
use Upload\Model\File as ModelFile;

/**
 * @Entity
 * @Table(name="TradeMaster_Category")
 * @Cache()
 */
class Category extends ORMFilterModel
{
    /**
     * @Id
     * @Column(type="integer")
     * @Filter\Lead\Integer();
     *
     * @var int
     */
    public $idZvena;

    /**
     * @Column(type="integer")
     * @Filter\Lead\Integer();
     *
     * @var integer
     */
    public $idParent;

    /**
     * @Column(type="string")
     * @Filter\Lead\Str();
     *
     * @var string
     */
    public $nameZvena;

    /**
     * @Column(type="string")
     * @Filter\Lead\Str();
     *
     * @var string
     */
    public $foto;

    /**
     * @Column(type="integer")
     * @Filter\Lead\Integer();
     *
     * @var integer
     */
    public $poryadok;

    /**
     * @Column(type="text")
     * @Filter\Lead\Str();
     *
     * @var string
     */
    public $opisanie;

    /**
     * @Column(type="string")
     * @Filter\Lead\Str();
     * @Filter\Lead\Regex('/\s/', '');
     *
     * @var string
     */
    public $link;

    /**
     * @Column(type="text")
     * @Filter\Lead\Str();
     *
     * @var string
     */
    public $ind1;

    /**
     * @Column(type="text")
     * @Filter\Lead\Str();
     *
     * @var string
     */
    public $ind2;

    /**
     * @Column(type="text")
     * @Filter\Lead\Str();
     *
     * @var string
     */
    public $ind3;

    /**
     * @Column(type="string", nullable=true)
     * @Filter\Lead\Str();
     *
     * @var string
     */
    public $changeDate;

    /**
     * @Column(type="text")
     * @Filter\Lead\Str();
     *
     * @var string
     */
    public $fullLink;

    /**
     * @Column(type="array", nullable=true)
     *
     * @var array
     */
    public $image_ids = [];

    /**
     * @var CollectionFile
     */
    public $images = null;

    /**
     * Загружает изображения
     *
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     * @throws \ReflectionException
     */
    public function processImages()
    {
        // загрузка изображения
        if (blank($this->image_ids) && !blank($this->foto)) {
            $images = explode(';', $this->foto);

            foreach ($images as $image) {
                /**
                 * @var ModelFile $file
                 */
                $file = upload_from_remote(tm_get_foto_path($image));

                if ($file) {
                    $this->image_ids[] = $file->id;

                    if (str_ends_with(['jpg', 'jpeg', 'png', 'gif'], $file->name)) {
                        $file->convert();
                    }
                }
            }
        }
    }

    /**
     * Возвращает изображения
     *
     * @return \ORMCollection
     */
    public function getImages()
    {
        if (!blank($this->images)) {
            return $this->images;
        }

        return $this->images = CollectionFile::fetch(['id' => $this->image_ids]);
    }

    /**
     * Возвращает первое изображение
     *
     * @param string $size
     * @param string $default
     *
     * @return mixed
     */
    public function firstImagePath($size = '', $default = '') {
        $this->getImages();

        if ($this->images->count()) {
            return $this->images->first()->path($size);
        }

        return $default;
    }

    /**
     * Возвращает родителей категории
     *
     * @param \TradeMaster\Collection\Category $catalog
     *
     * @return \AEngine\Orchid\Collection
     */
    public function parents($catalog = null)
    {
        if ($catalog === null) {
            $catalog = \TradeMaster\Collection\Category::fetch();
        }

        $result = collect();

        $buf = $this;

        while (true) {
            if (!$buf) break;

            $result->set(null, [
                'id' => $buf->idZvena,
                'name' => $buf->nameZvena,
                'link' => $buf->link . '/',
            ]);

            if ($buf->idParent == 0) {
                break;
            }

            $buf = $catalog->where('idZvena', '=', $buf->idParent)->first();
        }

        return $result->reverse();
    }

    /**
     * Возвращает путь категории
     *
     * @param \TradeMaster\Collection\Category $catalog
     *
     * @return string
     */
    public function path($catalog = null) {
        if ($catalog === null) {
            $catalog = \TradeMaster\Collection\Category::fetch();
        }

        $parents = $this->parents($catalog);

        return rtrim(app()->storage->value('module_trademaster', 'catalog') . '/' . $parents->pluck('link')->implode(''), '/');
    }

    /**
     * @deprecated use method parents()
     * @see parents()
     */
    public function getParents($catalog = null)
    {
        return $this->parents($catalog);
    }
}
