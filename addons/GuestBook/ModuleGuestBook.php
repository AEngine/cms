<?php

namespace GuestBook {

    use AEngine\Orchid\App;
    use AEngine\Orchid\Http\Request;
    use AEngine\Orchid\Http\Response;
    use GuestBook\Collection\Item as CollectionGBItem;
    use GuestBook\Model\Item as ModelGBItem;
    use Twig\TwigFunction as Twig_SimpleFunction;
    use Upload\Collection\File as CollectionFile;
    use Upload\Model\File as ModelFile;

    class ModuleGuestBook extends \CMSModule
    {
        public static function onInit(App $app)
        {
            // список отзывов
            static::addRoute(['get', 'post'], "#^\/guestbook\/?(?<offset>[0-9]*)#", function (Request $req, Response $res, $args) use ($app) {
                $default = [
                    'offset' => 0,
                ];
                $data = array_merge($default, $args[':capture']);

                if ($req->isPost()) {
                    $valid = true;
                    $item = new ModelGBItem([
                        'name' => $req->getParam('name', ''),
                        'email' => $req->getParam('email', ''),
                        'content' => $req->getParam('content', ''),
                    ]);
                    $check = $item->filter();

                    if ($req->getParam('recaptcha', false) !== false) {
                        $valid = \System::checkReCAPTCHA([
                            'response' => $req->getParam('recaptcha'),
                            'remoteip' => $req->getServerParam('REMOTE_ADDR'),
                        ]);
                    }

                    if ($check === true && $valid === true) {
                        $item->save();
                        $this->logger->info('GuestBook row has been added', ['id' => $item->id]);

                        return $res->withAddedHeader('Location', '/guestbook');
                    }
                    $this->view->getEnvironment()->addGlobal('_error', $check);
                }

                return render($req, $res, 'guestbook/list.html', [
                    'limit' => $this->storage->value('module_guestbook', 'pagination'),
                    'offset' => $data['offset'],
                ]);
            });

            if (panel_is_admin() === true) {
                // админ панель
                static::addRouteGroup('/cup/guestbook', function () {
                    // список отзывов
                    static::addRoute('get', '', function (Request $req, Response $res) {
                        return render($req, $res, 'guestbook/index.html', [
                            'list' => CollectionGBItem::fetch(['status' => 'work'], ['id' => 'desc'], $this->storage->value('module_guestbook', 'pagination')),
                        ]);
                    });

                    // редактирование записи
                    static::addRoute(['get', 'post'], '/:id/edit', function (Request $req, Response $res, $args) {
                        $item = ModelGBItem::fetch($args + ['status' => 'work']);

                        if ($item) {
                            if ($req->isPost()) {
                                $item->replace([
                                    'name' => $req->getParam('name', ''),
                                    'email' => $req->getParam('email', ''),
                                    'content' => $req->getParam('content', ''),
                                    'date' => $req->getParam('date', ''),
                                ]);
                                $check = $item->filter();

                                if ($check === true) {
                                    $item->save();

                                    if (($files = $req->getParam('file', false)) !== false) {
                                        $files = CollectionFile::fetch(['id' => $files]);

                                        /** @var ModelFile $file */
                                        foreach ($files as $file) {
                                            $file->item = 'guestbook';
                                            $file->item_id = $item->id;

                                            if (str_ends_with(['jpg', 'jpeg', 'png', 'gif'], $file->name)) {
                                                $file->convert();
                                            }

                                            $file->save();
                                        }
                                    }

                                    $this->logger->info('GuestBook row has been edited', ['id' => $item->id]);

                                    return $res->withAddedHeader('Location', '/cup/guestbook');
                                }
                                $this->view->getEnvironment()->addGlobal('_error', $check);
                            } else {
                                // для отрисовки шаблона
                                $_POST = $item->toArray();
                            }

                            return render($req, $res, 'guestbook/form.html', ['_post' => $_POST]);
                        }

                        return $res->withAddedHeader('Location', '/cup/guestbook');
                    });

                    // удаление записи
                    static::addRoute(['get', 'post'], '/:id/delete', function (Request $req, Response $res, $args) {
                        $item = ModelGBItem::fetch($args + ['status' => 'work']);

                        if ($item) {
                            $item->set('status', 'delete')->save();
                            $this->logger->info('GuestBook row has been mark as deleted', ['id' => $item->id]);
                        }

                        return $res->withAddedHeader('Location', '/cup/guestbook');
                    });

                });

                // пункт в меню
                panel_navbar_add([
                    'title' => 'Гостевая книга',
                    'icon' => 'fa-search',
                    'link' => '/guestbook',
                    'items' => [
                        [
                            'title' => 'Отзывы',
                        ]
                    ],
                    'position' => 10,
                ]);
            }
        }

        // настройки модуля
        public static function getSettings()
        {
            return [
                'title' => 'Гостевая книга',
                'description' => 'Позволяет пользователям оставлять отзывы на сайте',
                'params' => [
                    'pagination' => [
                        'title' => 'На страницу',
                        'description' => 'Количество отзывов отображаемых на странице',
                        'value' => 10,
                    ],
                ],
            ];
        }

        // функции модуля
        public static function getFunction()
        {
            return [
                new Twig_SimpleFunction('gb_fetch', function ($limit = null, $offset = null) {
                    $filter = [
                        'status' => 'work',
                    ];

                    if ($limit === null) {
                        $limit = app()->storage->value('module_guestbook', 'pagination');
                    }

                    return CollectionGBItem::fetch($filter, ['id' => 'desc'], $limit, $offset)->calcRowsCount();
                }),
            ];
        }

        public static function getStatistic()
        {
            return [
                [
                    'title' => 'Отзывов',
                    'value' => CollectionGBItem::fetch(['status' => 'work'])->count(),
                ],
            ];
        }
    }
}
