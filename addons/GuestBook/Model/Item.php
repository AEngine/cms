<?php

namespace GuestBook\Model;

use AEngine\Orchid\Filter as Filter;
use DateTime;
use Doctrine\ORM\Mapping\Cache;
use Doctrine\ORM\Mapping\Column;
use Doctrine\ORM\Mapping\Entity;
use Doctrine\ORM\Mapping\GeneratedValue;
use Doctrine\ORM\Mapping\Id;
use Doctrine\ORM\Mapping\Table;
use ORMFilterModel;
use Reference\Date as ReferenceDate;
use Upload\Collection\File as CollectionFile;

/**
 * @Entity
 * @Table(name="GuestBook")
 * @Cache()
 */
class Item extends ORMFilterModel
{
    /**
     * @Id
     * @Column(type="integer")
     * @GeneratedValue
     * @Filter\Lead\Integer()
     *
     * @var int
     */
    public $id;

    /**
     * @Column(type="string")
     * @Filter\Required()
     * @Filter\Lead\Str()
     * @Filter\Check\ValueNotEmpty(message="Поле не может быть пустым")
     *
     * @var string
     */
    public $name;

    /**
     * @Column(type="string", nullable=true)
     * @Filter\Required()
     * @Filter\Lead\Str()
     * @Filter\Check\Email()
     *
     * @var string
     */
    public $email;

    /**
     * @Column(type="text")
     * @Filter\Required()
     * @Filter\Lead\Str()
     * @Filter\Check\ValueNotEmpty(message="Поле не может быть пустым")
     *
     * @var string
     */
    public $content;

    /**
     * @Column(type="string")
     * @Filter\Lead\Str()
     * @Filter\Check\InValues({'work', 'delete'})
     *
     * @var string
     */
    public $status = 'work';

    /**
     * @Column(type="datetime")
     *
     * @var DateTime
     */
    public $date;

    /**
     * @var CollectionFile
     */
    public $files;

    public function replace(array $data)
    {
        parent::replace($data);

        if (is_string($this->date) || !$this->date) {
            $time = 'now';

            if ($this->date) {
                $time = date(ReferenceDate::DATETIME, strtotime($this->date));
            }

            $this->date = new DateTime($time);
        }

        return $this;
    }

    /**
     * Возвращает файлы
     *
     * @return \ORMCollection
     */
    public function getFiles()
    {
        if (!blank($this->files)) {
            return $this->files;
        }

        $this->files = CollectionFile::fetch(['item' => 'guestbook', 'item_id' => $this->id]);

        return $this->files;
    }
}
